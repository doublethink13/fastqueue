import os


def get_project_dir() -> str:
    project_dir = os.environ.get("CI_PROJECT_DIR")

    if project_dir is None:
        raise ValueError("CI_PROJECT_DIR env var is not available")

    return project_dir


def get_commit_branch() -> str:
    commit_branch = os.environ.get("CI_COMMIT_BRANCH")

    if commit_branch is None:
        raise ValueError("CI_COMMIT_BRANCH env var is not available")

    return commit_branch


def setup_env(project_dir: str, commit_branch: str) -> None:
    full_work_dir = f"{project_dir}/terraform/envs/"

    if commit_branch in {"shared", "dev", "staging", "main"}:
        full_work_dir += commit_branch
    else:
        full_work_dir += f"feature/{commit_branch}"

    print(f"export FULL_WORK_DIR={full_work_dir}")


def main():
    project_dir = get_project_dir()
    commit_branch = get_commit_branch()

    setup_env(project_dir, commit_branch)


if __name__ == "__main__":
    main()
