import secrets
import string
from datetime import datetime, timedelta

from cryptography.fernet import Fernet


def generate_bearer_token(username: str) -> str:
    return _sign_bearer_token(
        f"{username}:{_generate_expiry_date_timestamp()}:{_generate_random_string()}"
    )


def _sign_bearer_token(token: str) -> str:
    fernet = Fernet("Noor0ptm0J2X2FJaJcQFkfH7JsiZYP2drSjvGSBQM_k=")
    return fernet.encrypt(token.encode()).decode()


def _generate_random_string() -> str:
    characters = string.ascii_letters + string.digits

    return "".join(secrets.choice(characters) for _ in range(8))


def _generate_expiry_date_timestamp() -> float:
    now = datetime.now()
    expiry_date = now + timedelta(weeks=100000)

    return expiry_date.timestamp()


def main():
    print(generate_bearer_token("fastqueue"))


if __name__ == "__main__":
    main()
