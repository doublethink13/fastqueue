import json
import os


def get_env() -> str:
    env = os.environ.get("CI_COMMIT_BRANCH")

    if env is None:
        raise ValueError("CI_COMMIT_BRANCH is not present in env")

    return env


def get_image_url() -> str:
    return f"{os.environ.get('CI_REGISTRY_IMAGE')}:{os.environ.get('CI_COMMIT_BRANCH')}"


def update_tfvars(env: str, image_url: str) -> dict[str, str]:
    tfvars = {"env": env, "image_url": image_url}

    with open("terraform.tfvars.json", "w", encoding="utf-8") as tfvars_file:
        json.dump(
            tfvars,
            tfvars_file,
            ensure_ascii=False,
            indent=4,
        )

    return tfvars


def main():
    env = get_env()
    image_url = get_image_url()

    tfvars = update_tfvars(env, image_url)

    print(f"updated tfvars: {tfvars}")


if __name__ == "__main__":
    main()
