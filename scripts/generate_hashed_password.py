from passlib.context import CryptContext


def main():
    pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
    print(pwd_context.hash("averysecretpassword"))


if __name__ == "__main__":
    main()
