# Settings

The application is configured using environment variables. Every variable needs to have the `FASTQUEUE_` prefix. Below are all available configurable settings, with an explanation (if required), allowed values and the default value (if any).

## FASTQUEUE_DB_TYPE

Allowed values: `memory`.

Default: `memory`.

## FASTQUEUE_AUTHENTICATION_MODE

Allowed values: `off`, `bearer`.

Default: `off`.

## FASTQUEUE_BEARER_TOKEN_SIGNING_KEY

Allowed values: A `Fernet` key string.

Explanation: Check [this](https://gitlab.com/doublethink13/fastqueue/-/blob/main/scripts/generate_fernet_key.py) script on how to generate one. Check the Python `cryptography` package [docs](https://cryptography.io/en/latest/) for additional information on `Fernet` keys.

Default: `""`. Required if `FASTQUEUE_AUTHENTICATION_MODE == "bearer"`.

## FASTQUEUE_BEARER_TOKEN_VALIDITY_SECONDS

Allowed values: any int.

Default: `600`.

## FASTQUEUE_BEARER_TOKEN_USERNAME

Allowed values: any string.

Default: `""`. Required if `FASTQUEUE_AUTHENTICATION_MODE == "bearer"`.

## FASTQUEUE_BEARER_TOKEN_HASHED_PASSWORD

Allowed values: any string.

Explanation: Check [this](https://gitlab.com/doublethink13/fastqueue/-/blob/main/scripts/generate_hashed_password.py) script to generate an hashed password. Check the package [docs](https://passlib.readthedocs.io/en/stable/index.html) for additional information.

Default: `""`. Required if `FASTQUEUE_AUTHENTICATION_MODE == "bearer"`.

## FASTQUEUE_REMOVE_ITEMS_TASK_SECONDS

Allowed values: any int.

Explanation: how often the job to remove expired items runs.

Default: `"600"`

## FASTQUEUE_REMOVE_QUEUES_TASK_SECONDS

Allowed values: any int.

Explanation: how often the job to remove expired queues runs.

Default: `"1200"`
