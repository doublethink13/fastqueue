# Authorization

It's possible to protect queue routes with an authentication layer.

To do so, a set of env vars need to be defined. For an explanation on each env var, check the [Settings](./settings.md) section. For an example how to run a container with authorization on queue endpoints, run:

```
# PLEASE NEVER COMMIT YOUR SIGNING KEY TO SOURCE CODE!
docker run \
    --env FASTQUEUE_AUTHENTICATION_MODE="bearer" \
    --env FASTQUEUE_BEARER_TOKEN_SIGNING_KEY="Noor0ptm0J2X2FJaJcQFkfH7JsiZYP2drSjvGSBQM_k=" \
    --env FASTQUEUE_BEARER_TOKEN_USERNAME="fastqueue" \
    --env FASTQUEUE_BEARER_TOKEN_HASHED_PASSWORD="$2b$12$wbOIFJqNK4YY/cfFkAjkCuic.9YkKXrHLDQIwZAT4c/27ZUMWx6l2" \
    registry.gitlab.com/doublethink13/fastqueue:latest
```

You can find two scripts in the repository, [this one](https://gitlab.com/doublethink13/fastqueue/-/blob/main/scripts/generate_fernet_key.py) on how to generate a signing key and [another](https://gitlab.com/doublethink13/fastqueue/-/blob/main/scripts/generate_hashed_password.py) script to get an hashed password.

If you have ran the container without the authentication turned on, you can ignore the `Authorize` button. All endpoints will act has if they are not protected.

To get a valid token, POST to `/auth/token` with a valid `username` and `password` (the not hashed version!) as form-data. Use the retrieved access token in an `Authorization` header, with the `Bearer <access_token>` format.
