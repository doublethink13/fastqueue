# Fastqueue

A containerized application allowing you to create queues and add/remove items.

Uses an in memory database to store queues/items (for now).

Powered by FastAPI.
