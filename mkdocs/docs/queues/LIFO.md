# LIFO

Last in, first out

If you add `item1`, `item2` and `item3`, followed by three consecutive GET requests (asking for only one item), you would get `[item3]`, `[item2]` and `[item1]`. A GET request for three items would get you `[item3, item2, item1]`.
