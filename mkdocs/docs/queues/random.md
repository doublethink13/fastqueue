# Random

As the name implies, if you add `item1`, `item2` and `item3`, followed by three consecutive GET requests (asking for only one item), it's impossible to know the order in which the items would arrive (an array with one element would be returned). A GET request for three items would get you `[itemX, itemY, itemZ]` (where X, Y and Z would by 1, 2 or 3, excusevely).
