# Installation

Since this application is a Docker container, you simply have to download and run it wherever you like. You can find the various images available [here](https://gitlab.com/doublethink13/fastqueue/container_registry/3545790).

To download the latest version, run `docker pull registry.gitlab.com/doublethink13/fastqueue:latest`.

## Additional details

It uses `python:3.10.8` as a base image.

The app code lives in `/code/app`.

It runs using `CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]`.

Everything else is up to you. See the [Usage](./usage.md) section for additional information.
