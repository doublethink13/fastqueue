# Usage

This is a FastAPI powered application. As such, it comes with out of the box [Swagger UI](https://github.com/swagger-api/swagger-ui) interactive API documentation.

Running it locally, you could access them using `http://127.0.0.1:8000/docs`. The API docs have all the information needed to interact with the application. To get the [ReDoc](https://github.com/Rebilly/ReDoc) version, use the `/redoc` endpoint instead.

You can use a live version of the app [here](https://fastqueue.treuze.xyz).

You can:

- Create queues
- Delete queues
- Get one/all queue details
- Add one item to a queue
- Get x number of items from a queue

## Example

TODO (using live version)
