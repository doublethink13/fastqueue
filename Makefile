usage:
	@echo "Usage"
	@echo "	usage (default)"
	@echo "	make update_bashrc"
	@echo "	make echo_pass"
	@echo "	make setup_docker_socket"
	@echo "	make install"
	@echo "	make freeze"
	@echo "	make isort_check"
	@echo "	make isort_apply"
	@echo "	make lint"
	@echo "	make test_one_scripts TEST=<test_name>"
	@echo "	make test_one_unit TEST=<test_name>"
	@echo "	make test_one_int TEST=<test_name>"
	@echo "	make test_all_scripts"
	@echo "	make test_all_unit"
	@echo "	make test_all_int"
	@echo "	make test_cov_scripts"
	@echo "	make test_cov_unit"
	@echo "	make test_cov_int"
	@echo "	make test_cov_xml"
	@echo "	make run_local"

update_bashrc:
	@echo "cp ./.devcontainer/.bashrc ~/.bashrc && source ~/.bashrc"

echo_pass:
	@echo "alskdj"

setup_docker_socket: echo_pass
	@sudo chown python:python /var/run/docker.sock

install:
	@pip install -r requirements.txt

freeze:
	@pip freeze > requirements.txt

isort_check:
	@python -m isort . --check-only
	@python -m isort . --diff

isort_apply:
	@python -m isort .

lint:
	@python -m black . --check
	@python -m isort . --check-only
	@python -m flake8 .
	@python -m mypy .

test_one_scripts:
	@python -m pytest -s -k $(TEST) --ignore=tests/integration --ignore=tests/unit

test_one_unit:
	@python -m pytest -s -k $(TEST) --ignore=tests/integration --ignore=tests/scripts

test_one_int:
	@python -m pytest -s -k $(TEST) --ignore=tests/unit --ignore=tests/scripts

test_all_scripts: lint
	@python -m pytest -s --ignore=tests/integration --ignore=tests/unit

test_all_unit: lint
	@python -m pytest -s --ignore=tests/integration --ignore=tests/scripts

test_all_int: lint
	@python -m pytest --maxfail=1 -s --ignore=tests/unit --ignore=tests/scripts

test_cov_scripts: lint
	@python -m coverage run -m pytest --ignore=tests/integration --ignore=tests/unit
	@python -m coverage report --show-missing

test_cov_unit: lint
	@python -m coverage run -m pytest --ignore=tests/integration --ignore=tests/scripts
	@python -m coverage report --show-missing

test_cov_int: lint
	@python -m coverage run -m pytest --ignore=tests/unit --ignore=tests/scripts
	@python -m coverage report --show-missing

test_cov_xml:
	@python -m coverage xml

run_local:
	@python -m uvicorn app.main:app --reload