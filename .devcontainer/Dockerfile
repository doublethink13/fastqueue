FROM python:3.10.8

ARG USERNAME=python
ARG PASSWORD=alskdj
ARG DEBIAN_FRONTEND=noninteractive

USER root

RUN useradd \
        -u 1474 \
        -p $(openssl passwd -1 $PASSWORD) \
        -s /usr/bin/bash \
        --create-home \
        ${USERNAME} && \
    usermod -aG sudo ${USERNAME} && \
    echo "${USERNAME}:${PASSWORD}" | chpasswd

RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y \
        ca-certificates \
        curl \
        git \
        gnupg \
        groff \
        less \
        lsb-release \
        software-properties-common \
        sudo \
        wget \
        vim && \
        apt-get clean -y

RUN mkdir -p /etc/apt/keyrings && \
    curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
    echo \
        "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
        $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null && \
    apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install -y \
        docker-ce \
        docker-ce-cli \
        containerd.io \
        docker-compose-plugin

RUN wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | tee /usr/share/keyrings/hashicorp-archive-keyring.gpg && \
    gpg --no-default-keyring \
        --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg \
        --fingerprint && \
    echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list && \
    apt-get update -y && \
    apt-get install terraform=1.3.4 && \
    apt-get clean -y

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    rm -rf aws awscliv2.zip

COPY .bashrc /home/${USERNAME}

COPY .ssh /home/${USERNAME}/.ssh
RUN chmod 400 /home/${USERNAME}/.ssh/id* /home/${USERNAME}/.ssh/config

RUN chown -R ${USERNAME}:${USERNAME} /home/${USERNAME}

USER ${USERNAME}
