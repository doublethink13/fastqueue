from http import HTTPStatus
from uuid import UUID

from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse

from app.handlers.authorization.authorize import authorize
from app.handlers.item.create_items import create_items
from app.handlers.item.get_items import get_items
from app.handlers.queue.create_one import create_one_queue
from app.handlers.queue.delete_one import delete_one_queue
from app.handlers.queue.get_all_queues_details import get_all_queues_details
from app.handlers.queue.get_one_queue_details import get_one_queue_details
from app.models.managers.item_manager import ItemManager
from app.models.managers.queue_manager import QueueManager
from app.models.payloads.create_item import CreateItemPayload
from app.models.payloads.create_queue import CreateQueuePayload
from app.models.responses.item import ItemResponse
from app.models.responses.queue_details import QueueDetailResponse
from app.models.responses.unauthorized import UnauthorizedResponse

_router = APIRouter(
    prefix="/queue",
    tags=["queue"],
    dependencies=[Depends(authorize)],
    responses={HTTPStatus.UNAUTHORIZED.value: {"model": UnauthorizedResponse}},
)


def get_queue_router() -> APIRouter:
    return _router


@_router.post("", response_model=QueueDetailResponse)
def create_one_queue_handler(
    create_queue_payload: CreateQueuePayload,
    queue_manager: QueueManager = Depends(QueueManager),
) -> QueueDetailResponse:
    return create_one_queue(create_queue_payload, queue_manager)


@_router.get("/details/{queue_id}", response_model=QueueDetailResponse)
def get_one_queue_details_handler(
    queue_id: UUID,
    queue_manager: QueueManager = Depends(QueueManager),
) -> QueueDetailResponse:
    return get_one_queue_details(queue_id, queue_manager)


@_router.get("/details", response_model=list[QueueDetailResponse])
def get_all_queues_details_handler(
    queue_manager: QueueManager = Depends(QueueManager),
) -> list[QueueDetailResponse]:
    return get_all_queues_details(queue_manager)


@_router.delete("/{queue_id}", response_model=QueueDetailResponse)
def delete_one_queue_handler(
    queue_id: UUID,
    queue_manager: QueueManager = Depends(QueueManager),
) -> QueueDetailResponse:
    return delete_one_queue(queue_id, queue_manager)


@_router.post(
    "/{queue_id}",
    response_model=list[ItemResponse],
    responses={500: {"model": list[ItemResponse]}},
)
def create_items_handler(
    queue_id: UUID,
    create_items_payload: list[CreateItemPayload],
    item_manager: ItemManager = Depends(ItemManager),
) -> list[ItemResponse] | JSONResponse:
    created_items = create_items(queue_id, create_items_payload, item_manager)

    if len(created_items) == len(create_items_payload):
        return created_items
    else:
        return JSONResponse(content=created_items, status_code=500)


@_router.get("/{queue_id}", response_model=list[ItemResponse])
def get_items_handler(
    queue_id: UUID,
    number_of_items: int = 1,
    item_manager: ItemManager = Depends(ItemManager),
) -> list[ItemResponse]:
    return get_items(queue_id, number_of_items, item_manager)
