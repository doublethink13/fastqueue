from http import HTTPStatus

from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestFormStrict

from app.models.enums.authentication_failed import AuthenticationFailure
from app.models.managers.auth_manager import AuthManager
from app.models.responses.bearer_token import BearerTokenResponse
from app.models.responses.failed_authentication import FailedAuthenticationResponse
from app.models.types.failed_authentication import FailedAuthentication

_router = APIRouter(
    prefix="/auth",
    tags=["auth"],
)


def get_auth_router() -> APIRouter:
    return _router


@_router.post(
    "/token",
    response_model=BearerTokenResponse,
    responses={HTTPStatus.UNAUTHORIZED.value: {"model": FailedAuthenticationResponse}},
)
def get_token(
    form_data: OAuth2PasswordRequestFormStrict = Depends(),
    auth_manager: AuthManager = Depends(AuthManager.singleton),
) -> BearerTokenResponse:
    if not auth_manager.validate_username(form_data.username):
        raise HTTPException(
            status_code=HTTPStatus.UNAUTHORIZED,
            detail=FailedAuthentication(
                message="form data not valid",
                reason=AuthenticationFailure.WrongUsername,
            ).dict(),
        )

    if not auth_manager.validate_password(form_data.password):
        raise HTTPException(
            status_code=HTTPStatus.UNAUTHORIZED,
            detail=FailedAuthentication(
                message="form data not valid",
                reason=AuthenticationFailure.WrongPassword,
            ).dict(),
        )

    return BearerTokenResponse(
        access_token=auth_manager.generate_bearer_token(form_data.username)
    )
