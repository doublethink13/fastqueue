from http import HTTPStatus

from fastapi import Depends
from fastapi.exceptions import HTTPException

from app.models.managers.auth_manager import AuthManager
from app.models.types.unauthorized import Unauthorized


def authorize(
    token: str = Depends(AuthManager.oauth2_scheme),
    auth_manager: AuthManager = Depends(AuthManager.singleton),
) -> None:
    if not auth_manager.is_auth_active():
        return

    error = auth_manager.validate_bearer_token(token)

    if error is not None:
        raise HTTPException(
            status_code=HTTPStatus.UNAUTHORIZED,
            detail=Unauthorized(message="Bearer token not valid", reason=error).dict(),
        )
