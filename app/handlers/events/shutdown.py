from app.models.managers.db_manager import DbManager


def shutdown() -> None:
    DbManager().delete_db_connection()
