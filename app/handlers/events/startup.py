from threading import Thread

from app.models.managers.auth_manager import AuthManager
from app.models.managers.db_manager import DbManager
from app.models.managers.task_manager import TaskManager
from app.settings import Settings


def startup() -> None:
    Settings.singleton(new=True)

    AuthManager.singleton(new=True)

    DbManager.get_db_manager()

    Thread(target=TaskManager.task_running_startup, args=(), daemon=True).start()
