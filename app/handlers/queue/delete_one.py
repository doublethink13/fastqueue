from http import HTTPStatus
from uuid import UUID

from fastapi import HTTPException

from app.models.managers.queue_manager import QueueManager
from app.models.responses.queue_details import QueueDetailResponse


def delete_one_queue(
    queue_id: UUID, queue_manager: QueueManager
) -> QueueDetailResponse:
    deleted_queue = queue_manager.delete_one(queue_id)

    if deleted_queue is not None:
        return QueueDetailResponse(
            queue_id=deleted_queue.queue_id,
            name=deleted_queue.name,
            description=deleted_queue.description,
            expiry_date=deleted_queue.expiry_date,
            queue_type=deleted_queue.queue_type,
            number_of_items=len(deleted_queue.items),
        )
    else:
        raise HTTPException(
            status_code=HTTPStatus.NOT_FOUND,
            detail=f"queue {queue_id} not found",
        )
