from http import HTTPStatus
from uuid import UUID

from fastapi import HTTPException

from app.models.managers.queue_manager import QueueManager
from app.models.responses.queue_details import QueueDetailResponse


def get_one_queue_details(
    queue_id: UUID, queue_manager: QueueManager
) -> QueueDetailResponse:
    queue = queue_manager.get_one(queue_id)

    if queue is not None:
        return QueueDetailResponse(
            queue_id=queue.queue_id,
            name=queue.name,
            description=queue.description,
            expiry_date=queue.expiry_date,
            queue_type=queue.queue_type,
            number_of_items=len(queue.items),
        )
    else:
        raise HTTPException(
            status_code=HTTPStatus.NOT_FOUND,
            detail=f"queue {queue_id} not found",
        )
