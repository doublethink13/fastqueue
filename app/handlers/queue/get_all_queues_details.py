from app.models.managers.queue_manager import QueueManager
from app.models.responses.queue_details import QueueDetailResponse


def get_all_queues_details(queue_manager: QueueManager) -> list[QueueDetailResponse]:
    all_queues = queue_manager.get_all_queues()

    return [
        QueueDetailResponse(
            queue_id=queue.queue_id,
            name=queue.name,
            description=queue.description,
            expiry_date=queue.expiry_date,
            queue_type=queue.queue_type,
            number_of_items=len(queue.items),
        )
        for queue in all_queues
    ]
