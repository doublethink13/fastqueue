from app.models.managers.queue_manager import QueueManager
from app.models.payloads.create_queue import CreateQueuePayload
from app.models.responses.queue_details import QueueDetailResponse


def create_one_queue(
    create_queue_payload: CreateQueuePayload, queue_manager: QueueManager
) -> QueueDetailResponse:
    queue = queue_manager.create(create_queue_payload)

    return QueueDetailResponse(
        queue_id=queue.queue_id,
        name=queue.name,
        description=queue.description,
        expiry_date=queue.expiry_date,
        queue_type=queue.queue_type,
        number_of_items=len(queue.items),
    )
