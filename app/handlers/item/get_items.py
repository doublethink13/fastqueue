from http import HTTPStatus
from uuid import UUID

from fastapi import HTTPException

from app.models.exceptions.no_item_in_queue import NoItemInQueueException
from app.models.exceptions.no_queue_found import NoQueueFoundException
from app.models.managers.item_manager import ItemManager
from app.models.responses.item import ItemResponse


def get_items(
    queue_id: UUID, number_of_items: int, item_manager: ItemManager
) -> list[ItemResponse]:
    items, error = item_manager.get_items_from_queue(queue_id, number_of_items)

    if isinstance(error, NoQueueFoundException):
        raise HTTPException(status_code=HTTPStatus.NOT_FOUND, detail=error.message)

    if isinstance(error, NoItemInQueueException):
        raise HTTPException(
            status_code=HTTPStatus.INTERNAL_SERVER_ERROR, detail=error.message
        )

    return [
        ItemResponse(
            item_id=item.item_id,
            name=item.name,
            description=item.description,
            expiry_date=item.expiry_date,
            data=item.data,
        )
        for item in items
    ]
