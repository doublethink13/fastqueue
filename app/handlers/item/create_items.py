from http import HTTPStatus
from uuid import UUID

from fastapi.exceptions import HTTPException

from app.models.exceptions.no_queue_found import NoQueueFoundException
from app.models.managers.item_manager import ItemManager
from app.models.payloads.create_item import CreateItemPayload
from app.models.responses.item import ItemResponse


def create_items(
    queue_id: UUID,
    create_items_payload: list[CreateItemPayload],
    item_manager: ItemManager,
) -> list[ItemResponse]:
    items: list[ItemResponse] = []

    for create_item_payload in create_items_payload:
        item, error = item_manager.add_item_to_queue(queue_id, create_item_payload)

        if isinstance(error, NoQueueFoundException):
            raise HTTPException(status_code=HTTPStatus.NOT_FOUND, detail=error.message)

        if item is not None:
            items.append(
                ItemResponse(
                    item_id=item.item_id,
                    name=item.name,
                    description=item.description,
                    expiry_date=item.expiry_date,
                    data=item.data,
                )
            )

    return items
