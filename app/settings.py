from pydantic import BaseSettings, Field

from app.models.enums.authentication_mode import AuthenticationMode
from app.models.enums.db_type import DbType


class Settings(BaseSettings):
    _singleton = None

    @classmethod
    def singleton(cls, new: bool = False) -> "Settings":
        if cls._singleton is None or new:
            cls._singleton = Settings()
        return cls._singleton

    remove_items_task_seconds: int = Field(default=600)
    remove_queues_task_seconds: int = Field(default=3600)

    authentication_mode: AuthenticationMode = Field(default=AuthenticationMode.Off)

    bearer_token_validity_seconds: int = Field(default=600)
    bearer_token_signing_key: str = Field(default="")
    bearer_token_username: str = Field(default="")
    bearer_token_hashed_password: str = Field(default="")

    db_type: DbType = Field(default=DbType.Memory)

    class Config:  # type:ignore
        env_file = ".env"
        env_file_encoding = "utf-8"
        env_prefix = "fastqueue_"
