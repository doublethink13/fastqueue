import time

import schedule  # type:ignore

from app.models.managers.queue_manager import QueueManager
from app.settings import Settings


class TaskManager:
    @classmethod
    def task_running_startup(cls) -> None:
        settings = Settings.singleton()

        schedule.every(settings.remove_items_task_seconds).seconds.do(  # type:ignore
            cls._remove_all_items
        )

        schedule.every(settings.remove_queues_task_seconds).seconds.do(  # type:ignore
            cls._remove_all_queues
        )

        while True:
            schedule.run_pending()
            time.sleep(1)

    @classmethod
    def _remove_all_items(cls) -> None:
        queue_manager = QueueManager()

        queue_manager.remove_expired_items_from_all_queues()

    @classmethod
    def _remove_all_queues(cls) -> None:
        queue_manager = QueueManager()

        queue_manager.remove_expired_queues()
