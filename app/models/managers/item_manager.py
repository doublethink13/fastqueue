from uuid import UUID

from app.models.exceptions.no_queue_found import NoQueueFoundException
from app.models.exceptions.unexpected_exception import UnexpectedException
from app.models.items.item import Item
from app.models.managers.queue_manager import QueueManager
from app.models.payloads.create_item import CreateItemPayload

AddItemToQueueErrors = NoQueueFoundException
AddItemToQueueReturnValues = tuple[Item | None, AddItemToQueueErrors | None]

GetItemsFromQueueError = NoQueueFoundException | UnexpectedException
GetItemsFromQueueReturnValues = tuple[list[Item], GetItemsFromQueueError | None]


class ItemManager:
    def __init__(self) -> None:
        self._queue_manager = QueueManager()

    def add_item_to_queue(
        self, queue_id: UUID, create_item_payload: CreateItemPayload
    ) -> AddItemToQueueReturnValues:
        queue = self._queue_manager.get_one(queue_id)

        if queue is None:
            return None, NoQueueFoundException(f"queue {queue_id} not found")

        item = Item(
            name=create_item_payload.name,
            description=create_item_payload.description,
            expiry_date=create_item_payload.expiry_date,
            data=create_item_payload.data,
        )

        queue.add_item(item)

        return item, None

    def get_items_from_queue(
        self, queue_id: UUID, number_of_items: int
    ) -> GetItemsFromQueueReturnValues:
        queue = self._queue_manager.get_one(queue_id)

        if queue is None:
            return [], NoQueueFoundException(f"queue {queue_id} not found")

        return queue.get_items(number_of_items), None
