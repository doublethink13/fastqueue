from uuid import UUID

from app.models.dbs.in_memory import InMemoryDb
from app.models.dbs.interface import DbInterface
from app.models.queues.base_queue import BaseQueue


class DbManager:
    def __init__(self) -> None:
        self._db: DbInterface | None = None

    @classmethod
    def get_db_manager(cls) -> "DbManager":
        db_manager = DbManager()

        db_manager.setup_db()

        return db_manager

    def setup_db(self) -> None:
        self._db = self._get_db()

    def _get_db(self) -> DbInterface:
        return InMemoryDb.singleton()

    def delete_db_connection(self) -> None:
        if self._db is not None:
            self._db.delete_connection()

    def save_queue(self, queue: BaseQueue) -> BaseQueue:
        if self._db is None:
            self._db = self._get_db()

        return self._db.save_queue(queue)

    def get_queue(self, queue_id: UUID) -> BaseQueue | None:
        if self._db is None:
            self._db = self._get_db()

        return self._db.get_queue(queue_id)

    def get_all_queues(self) -> list[BaseQueue]:
        if self._db is None:
            self._db = self._get_db()

        return self._db.get_all_queues()

    def delete_queue(self, queue_id: UUID) -> BaseQueue | None:
        if self._db is None:
            self._db = self._get_db()

        return self._db.delete_queue(queue_id)
