from datetime import datetime
from uuid import UUID

from app.models.factories.queue_factory import QueueFactory
from app.models.managers.db_manager import DbManager
from app.models.payloads.create_queue import CreateQueuePayload
from app.models.queues.base_queue import BaseQueue


class QueueManager:
    def __init__(self) -> None:
        self._db_manager = self._get_db_manager()
        self._queue_factory = QueueFactory()

    def _get_db_manager(self) -> DbManager:
        return DbManager()

    def create(self, create_queue_payload: CreateQueuePayload) -> BaseQueue:
        return self._db_manager.save_queue(
            self._queue_factory.create_queue(create_queue_payload)
        )

    def get_one(self, queue_id: UUID) -> BaseQueue | None:
        return self._db_manager.get_queue(queue_id)

    def get_all_queues(self) -> list[BaseQueue]:
        return self._db_manager.get_all_queues()

    def delete_one(self, queue_id: UUID) -> BaseQueue | None:
        return self._db_manager.delete_queue(queue_id)

    def remove_expired_items_from_all_queues(self) -> None:
        for queue in self.get_all_queues():
            queue.remove_expired_items()

    def remove_expired_queues(self) -> None:
        for queue in self.get_all_queues():
            if (
                queue.expiry_date is not None
                and queue.expiry_date.timestamp() < datetime.now().timestamp()
            ):
                self.delete_one(queue.queue_id)
