import secrets
import string
from datetime import datetime, timedelta

from cryptography.fernet import Fernet
from fastapi.security import OAuth2PasswordBearer
from passlib.context import CryptContext

from app.models.enums.authentication_mode import AuthenticationMode
from app.models.enums.bearer_token_failed_validation import BearerTokenFailedValidation
from app.models.exceptions.auth_settings import AuthSettingsException
from app.models.types.auth_settings import AuthSettings
from app.settings import Settings


class AuthManager:
    oauth2_scheme = OAuth2PasswordBearer(tokenUrl="auth/token", auto_error=False)

    _singleton = None

    @classmethod
    def singleton(cls, new: bool = False) -> "AuthManager":
        if cls._singleton is None or new:
            cls._singleton = AuthManager()
        return cls._singleton

    def __init__(self) -> None:
        settings = Settings.singleton()

        self._validate_auth_settings(settings)

        self._authentication_mode = settings.authentication_mode
        self._characters = string.ascii_letters + string.digits
        self._passlib = CryptContext(schemes=["bcrypt"], deprecated="auto")
        self._bearer_token_validity_seconds = settings.bearer_token_validity_seconds
        self._bearer_token_signing_key = settings.bearer_token_signing_key
        self._username = settings.bearer_token_username
        self._hashed_password = settings.bearer_token_hashed_password
        self._fernet = Fernet(self._bearer_token_signing_key)

    def is_auth_active(self) -> bool:
        return self._authentication_mode != AuthenticationMode.Off

    def validate_username(self, username: str) -> bool:
        return username == self._username

    def validate_password(self, password: str) -> bool:
        return self._passlib.verify(password, self._hashed_password)

    def generate_bearer_token(self, username: str) -> str:
        return self._sign_bearer_token(
            f"{username}:"
            + f"{self._generate_expiry_date_timestamp()}:"
            + f"{self._generate_random_string()}"
        )

    def validate_bearer_token(
        self, bearer_token: str
    ) -> BearerTokenFailedValidation | None:
        try:
            decrypted_token = self._fernet.decrypt(bearer_token).decode()
        except Exception:
            return BearerTokenFailedValidation.InvalidFormat

        username, expiry_date = self._extract_bearer_token_information(decrypted_token)

        if username != self._username:
            return BearerTokenFailedValidation.InvalidUsername

        if datetime.now().timestamp() > expiry_date:
            return BearerTokenFailedValidation.Expired

        return None

    def _validate_auth_settings(self, settings: Settings) -> None:
        if settings.authentication_mode == AuthenticationMode.Bearer and (
            settings.bearer_token_signing_key == ""
            or settings.bearer_token_username == ""
            or settings.bearer_token_hashed_password == ""
        ):
            auth_settings_dict = self._get_auth_settings(settings).dict()

            auth_settings_dict["authentication_mode"] = auth_settings_dict[
                "authentication_mode"
            ].value

            raise AuthSettingsException(
                f"Auth settings are not valid: {auth_settings_dict}"
            )

    def _get_auth_settings(self, settings: Settings) -> AuthSettings:
        return AuthSettings(
            authentication_mode=settings.authentication_mode,
            bearer_token_validity_seconds=settings.bearer_token_validity_seconds,
            bearer_token_signing_key=settings.bearer_token_signing_key,
            bearer_token_username=settings.bearer_token_username,
            bearer_token_hashed_password=settings.bearer_token_hashed_password,
        )

    def _sign_bearer_token(self, token: str) -> str:
        return self._fernet.encrypt(token.encode()).decode() if self._fernet else ""

    def _generate_random_string(self) -> str:
        return "".join(secrets.choice(self._characters) for _ in range(8))

    def _generate_expiry_date_timestamp(self) -> float:
        now = datetime.now()
        expiry_date = now + timedelta(seconds=self._bearer_token_validity_seconds)

        return expiry_date.timestamp()

    def _extract_bearer_token_information(self, bearer_token: str) -> tuple[str, float]:
        information_as_list = bearer_token.split(":")

        if len(information_as_list) != 3:
            return "", 1
        else:
            return information_as_list[0], float(information_as_list[1])
