from pprint import PrettyPrinter
from uuid import UUID

from app.models.dbs.interface import DbInterface
from app.models.queues.base_queue import BaseQueue


class InMemoryDb(DbInterface):
    _singleton = None

    @classmethod
    def singleton(cls) -> DbInterface:
        if cls._singleton is None:
            cls._singleton = InMemoryDb()
        return cls._singleton

    @classmethod
    def _remove_singleton(cls) -> None:
        cls._singleton = None

    def __init__(self) -> None:
        self._queues: dict[UUID, BaseQueue] = {}

        self._pretty_print = PrettyPrinter()

    def __str__(self) -> str:
        return self._pretty_print.pformat(vars(self))

    def delete_connection(self) -> None:
        InMemoryDb._remove_singleton()

    def save_queue(self, queue: BaseQueue) -> BaseQueue:
        self._queues[queue.queue_id] = queue

        return queue

    def get_queue(self, queue_id: UUID) -> BaseQueue | None:
        return self._queues.get(queue_id)

    def get_all_queues(self) -> list[BaseQueue]:
        return list(self._queues.values())

    def delete_queue(self, queue_id: UUID) -> BaseQueue | None:
        return self._queues.pop(queue_id, None)
