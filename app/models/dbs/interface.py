from abc import ABC, abstractmethod
from uuid import UUID

from app.models.queues.base_queue import BaseQueue


class DbInterface(ABC):
    @abstractmethod
    def delete_connection(self) -> None:
        pass

    @abstractmethod
    def save_queue(self, queue: BaseQueue) -> BaseQueue:
        pass

    @abstractmethod
    def get_queue(self, queue_id: UUID) -> BaseQueue | None:
        pass

    @abstractmethod
    def get_all_queues(self) -> list[BaseQueue]:
        pass

    @abstractmethod
    def delete_queue(self, queue_id: UUID) -> BaseQueue | None:
        pass
