from datetime import datetime
from pprint import PrettyPrinter
from uuid import uuid4

from app.models.types.json import TypeJSON


class Item:
    def __init__(
        self,
        name: str,
        description: str,
        expiry_date: datetime | None,
        data: TypeJSON,
    ) -> None:
        self.item_id = uuid4()
        self.name = name
        self.description = description
        self.expiry_date = expiry_date
        self.data = data

        self._pretty_print = PrettyPrinter()

    def __str__(self) -> str:
        return self._pretty_print.pformat(vars(self))
