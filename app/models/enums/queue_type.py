from enum import Enum


class QueueType(Enum):
    FIFO = "fifo"
    LIFO = "lifo"
    Random = "random"
