from enum import Enum


class AuthenticationMode(Enum):
    Off = "off"
    Bearer = "bearer"
