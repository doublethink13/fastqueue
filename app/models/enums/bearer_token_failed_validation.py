from enum import Enum


class BearerTokenFailedValidation(str, Enum):
    InvalidFormat = "invalid_format"
    InvalidUsername = "invalid_username"
    Expired = "expired"
