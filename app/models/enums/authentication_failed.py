from enum import Enum


class AuthenticationFailure(str, Enum):
    WrongPassword = "wrong_password"
    WrongUsername = "wrong_username"
