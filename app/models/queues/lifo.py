from datetime import datetime

from app.models.enums.queue_type import QueueType
from app.models.items.item import Item
from app.models.queues.base_queue import BaseQueue


class LifoQueue(BaseQueue):
    def __init__(
        self,
        name: str,
        description: str,
        expiry_date: datetime | None,
    ) -> None:
        super().__init__(name, description, expiry_date, QueueType.LIFO)

    def add_item(self, item: Item) -> None:
        self.items.append(item)

    def get_items(self, number_of_items: int = 1) -> list[Item]:
        items: list[Item] = []

        for _ in range(number_of_items):
            try:
                items.append(self.items.pop())
            except Exception:
                break

        return items
