from datetime import datetime
from random import randint

from app.models.enums.queue_type import QueueType
from app.models.items.item import Item
from app.models.queues.base_queue import BaseQueue


class RandomQueue(BaseQueue):
    def __init__(
        self,
        name: str,
        description: str,
        expiry_date: datetime | None,
    ) -> None:
        super().__init__(name, description, expiry_date, QueueType.Random)

    def add_item(self, item: Item) -> None:
        self.items.append(item)

    def get_items(self, number_of_items: int = 1) -> list[Item]:
        items: list[Item] = []

        for _ in range(number_of_items):
            try:
                random_index = randint(0, len(self.items) - 1)

                items.append(self.items.pop(random_index))
            except Exception:
                break

        return items
