from abc import ABC, abstractmethod
from datetime import datetime
from pprint import PrettyPrinter
from uuid import uuid4

from app.models.enums.queue_type import QueueType
from app.models.items.item import Item


class BaseQueue(ABC):
    def __init__(
        self,
        name: str,
        description: str,
        expiry_date: datetime | None,
        queue_type: QueueType,
    ) -> None:
        self.queue_id = uuid4()
        self.name = name
        self.description = description
        self.expiry_date = expiry_date
        self.queue_type = queue_type
        self.items: list[Item] = []

        self._pretty_print = PrettyPrinter()

    def __str__(self) -> str:
        return self._pretty_print.pformat(vars(self))

    @abstractmethod
    def add_item(self, item: Item) -> None:
        pass

    @abstractmethod
    def get_items(self, number_of_items: int = 1) -> list[Item]:
        pass

    def size(self) -> int:
        return len(self.items)

    def remove_expired_items(self) -> None:
        self.items = [
            item
            for item in self.items
            if item.expiry_date is None
            or item.expiry_date.timestamp() > datetime.now().timestamp()
        ]
