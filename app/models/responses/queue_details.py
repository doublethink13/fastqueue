from datetime import datetime
from uuid import UUID

from pydantic import BaseModel, Field

from app.models.enums.queue_type import QueueType


class QueueDetailResponse(BaseModel):
    queue_id: UUID = Field(...)
    name: str = Field(...)
    description: str = Field(...)
    expiry_date: datetime | None = Field(...)
    queue_type: QueueType = Field(...)
    number_of_items: int = Field(...)

    class Config:
        fields = {"queue_id": {"alias": "id"}, "queue_type": {"alias": "type"}}
        allow_population_by_field_name = True
