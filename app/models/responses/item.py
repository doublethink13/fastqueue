from datetime import datetime
from uuid import UUID

from pydantic import BaseModel, Field

from app.models.types.json import TypeJSON


class ItemResponse(BaseModel):
    item_id: UUID = Field(...)
    name: str = Field(...)
    description: str = Field(...)
    expiry_date: datetime | None = Field(...)
    data: TypeJSON = Field(...)

    class Config:
        fields = {"item_id": {"alias": "id"}}
        allow_population_by_field_name = True
