from pydantic import BaseModel

from app.models.types.failed_authentication import FailedAuthentication


class FailedAuthenticationResponse(BaseModel):
    detail: FailedAuthentication
