from pydantic import BaseModel

from app.models.types.unauthorized import Unauthorized


class UnauthorizedResponse(BaseModel):
    detail: Unauthorized
