from pydantic import BaseModel

from app.models.enums.bearer_token_failed_validation import BearerTokenFailedValidation


class Unauthorized(BaseModel):
    message: str
    reason: BearerTokenFailedValidation
