from pydantic import BaseModel

from app.models.enums.authentication_mode import AuthenticationMode


class AuthSettings(BaseModel):
    authentication_mode: AuthenticationMode
    bearer_token_validity_seconds: int
    bearer_token_signing_key: str
    bearer_token_username: str
    bearer_token_hashed_password: str
