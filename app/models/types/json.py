from typing import Any, Dict, List, Type, Union

JSON = Union[Dict[str, Any], List[Any], int, str, float, bool, Type[None]]

TypeJSON = Union[Dict[str, JSON], List[JSON], int, str, float, bool, Type[None]]
