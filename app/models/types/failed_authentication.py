from pydantic import BaseModel

from app.models.enums.authentication_failed import AuthenticationFailure


class FailedAuthentication(BaseModel):
    message: str
    reason: AuthenticationFailure
