from app.models.enums.queue_type import QueueType
from app.models.payloads.create_queue import CreateQueuePayload
from app.models.queues.base_queue import BaseQueue
from app.models.queues.fifo import FifoQueue
from app.models.queues.lifo import LifoQueue
from app.models.queues.random import RandomQueue


class QueueFactory:
    def create_queue(self, create_queue_payload: CreateQueuePayload) -> BaseQueue:
        if create_queue_payload.queue_type == QueueType.LIFO:
            return self._create_lifo_queue(create_queue_payload)
        elif create_queue_payload.queue_type == QueueType.FIFO:
            return self._create_fifo_queue(create_queue_payload)
        else:
            return self._create_random_queue(create_queue_payload)

    def _create_lifo_queue(self, create_queue_payload: CreateQueuePayload) -> BaseQueue:
        return LifoQueue(
            create_queue_payload.name,
            create_queue_payload.description,
            create_queue_payload.expiry_date,
        )

    def _create_fifo_queue(self, create_queue_payload: CreateQueuePayload) -> BaseQueue:
        return FifoQueue(
            create_queue_payload.name,
            create_queue_payload.description,
            create_queue_payload.expiry_date,
        )

    def _create_random_queue(
        self, create_queue_payload: CreateQueuePayload
    ) -> BaseQueue:
        return RandomQueue(
            create_queue_payload.name,
            create_queue_payload.description,
            create_queue_payload.expiry_date,
        )
