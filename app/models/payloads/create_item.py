from datetime import datetime

from pydantic import BaseModel, Field

from app.models.types.json import TypeJSON


class CreateItemPayload(BaseModel):
    name: str = Field(...)
    description: str = Field(...)
    expiry_date: datetime | None = Field(default=None)
    data: TypeJSON = Field(...)
