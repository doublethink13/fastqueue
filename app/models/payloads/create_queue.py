from datetime import datetime

from pydantic import BaseModel, Field

from app.models.enums.queue_type import QueueType


class CreateQueuePayload(BaseModel):
    name: str = Field(...)
    description: str = Field(...)
    expiry_date: datetime | None = Field(default=None)
    queue_type: QueueType = Field(...)

    class Config:
        fields = {"queue_type": {"alias": "type"}}
        allow_population_by_field_name = True
