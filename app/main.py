from fastapi import FastAPI

from app.handlers.events.shutdown import shutdown
from app.handlers.events.startup import startup
from app.routers.auth import get_auth_router
from app.routers.queue import get_queue_router


def create_application() -> FastAPI:
    app = FastAPI()

    app.include_router(get_queue_router())
    app.include_router(get_auth_router())

    @app.on_event("startup")  # type:ignore
    def startup_handler() -> None:  # type:ignore
        startup()

    @app.on_event("shutdown")  # type:ignore
    def shutdown_handler() -> None:  # type:ignore
        shutdown()

    @app.get("/")
    def root_handler() -> dict[str, str]:  # type:ignore
        return {"message": "Hello, world!"}

    return app


app = create_application()
