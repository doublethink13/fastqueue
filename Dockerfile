FROM python:3.10.8
 
WORKDIR /code
 
RUN pip install \
    "fastapi[all]==0.85.1" \
    "cryptography==38.0.4" \
    "passlib==1.7.4" \
    "schedule==1.1.0" \
    --no-cache-dir \
    --upgrade
 
COPY ./app /code/app
 
CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]
