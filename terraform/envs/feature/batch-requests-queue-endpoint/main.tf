variable "image_url" {
  type = string
}

variable "env" {
  type = string
}

module "fastqueue_load_data" {
  source = "../../../modules/load_data"

  env = var.env
}

module "fastqueue_fargate_deployment" {
  source = "../../../modules/fargate"

  app_name = "fastqueue-batch-requests-queue"
  env      = var.env

  vpc_fastqueue_id   = module.fastqueue_load_data.vpc_fastqueue_id
  vpc_fastqueue_cidr = module.fastqueue_load_data.vpc_fastqueue_cidr

  subnets_ids_public  = module.fastqueue_load_data.subnets_ids_public
  subnets_ids_private = module.fastqueue_load_data.subnets_ids_private

  image_url = var.image_url

  certificate_arn             = module.fastqueue_load_data.certificate_arn
  route_53_treuze_xyz_zone_id = module.fastqueue_load_data.route_53_treuze_xyz_zone_id
}

output "load_balancer_address" {
  value = module.fastqueue_fargate_deployment.load_balancer_address
}
