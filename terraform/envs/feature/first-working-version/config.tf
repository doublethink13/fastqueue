terraform {
  backend "s3" {
    bucket = "fastqueue-tfstate"
    key    = "fastqueue.first-working-version.tfstate"
    region = "eu-central-1"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.16"
    }
  }

  required_version = "1.3.4"
}

provider "aws" {
  region = "eu-central-1"

  default_tags {
    tags = {
      Project   = "fastqueue"
      Terraform = "true"
      Env       = var.env
    }
  }
}
