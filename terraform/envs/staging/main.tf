variable "image_url" {
  type = string
}

variable "env" {
  type = string
}

module "fastqueue_load_data" {
  source = "../../modules/load_data"

  env = var.env
}

module "fastqueue_fargate_deployment" {
  source = "../../modules/fargate"

  app_name = "fastqueue-staging"
  env      = var.env

  vpc_fastqueue_id   = module.fastqueue_load_data.vpc_fastqueue_id
  vpc_fastqueue_cidr = module.fastqueue_load_data.vpc_fastqueue_cidr

  subnets_ids_public  = module.fastqueue_load_data.subnets_ids_public
  subnets_ids_private = module.fastqueue_load_data.subnets_ids_private

  image_url = var.image_url

  certificate_arn             = module.fastqueue_load_data.certificate_arn
  route_53_treuze_xyz_zone_id = module.fastqueue_load_data.route_53_treuze_xyz_zone_id

  # NOT PRODUCTION SAFE
  # PLEASE INJECT THESE VALUES SAFELY. DO NOT COMMIT THEM TO SOURCE CODE.
  container_env = {
    FASTQUEUE_DB_TYPE                       = "memory"
    FASTQUEUE_AUTHENTICATION_MODE           = "bearer"
    FASTQUEUE_BEARER_TOKEN_VALIDITY_SECONDS = "600"
    FASTQUEUE_BEARER_TOKEN_SIGNING_KEY      = "Noor0ptm0J2X2FJaJcQFkfH7JsiZYP2drSjvGSBQM_k="
    FASTQUEUE_BEARER_TOKEN_USERNAME         = "fastqueue"
    FASTQUEUE_BEARER_TOKEN_HASHED_PASSWORD  = "$2b$12$wbOIFJqNK4YY/cfFkAjkCuic.9YkKXrHLDQIwZAT4c/27ZUMWx6l2"
    FASTQUEUE_REMOVE_ITEMS_TASK_SECONDS     = "60"
    FASTQUEUE_REMOVE_QUEUES_TASK_SECONDS    = "60"
  }
}

output "load_balancer_address" {
  value = module.fastqueue_fargate_deployment.load_balancer_address
}
