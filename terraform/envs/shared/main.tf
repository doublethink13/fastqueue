variable "env" {
  type = string
}

module "fastqueue_network_deployment" {
  source = "../../modules/network"
}
