output "subnets_ids_private" {
  value = [for k, v in data.aws_subnet.fastqueue_private : data.aws_subnet.fastqueue_private[k].id]
}
