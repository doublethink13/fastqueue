data "aws_subnets" "fastqueue_public" {
  filter {
    name   = "tag:SubnetType"
    values = ["public"]
  }

  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.fastqueue.id]
  }
}

data "aws_subnet" "fastqueue_public" {
  for_each = toset(data.aws_subnets.fastqueue_public.ids)
  id       = each.value
}

data "aws_subnets" "fastqueue_private" {
  filter {
    name   = "tag:SubnetType"
    values = ["private"]
  }

  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.fastqueue.id]
  }
}

data "aws_subnet" "fastqueue_private" {
  for_each = toset(data.aws_subnets.fastqueue_private.ids)
  id       = each.value
}
