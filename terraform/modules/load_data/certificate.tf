data "aws_acm_certificate" "fastqueue" {
  count = var.env == "main" || var.env == "staging" || var.env == "dev" ? 1 : 0

  domain      = var.env == "main" ? "*.treuze.xyz" : "*.${var.env}.treuze.xyz"
  types       = ["AMAZON_ISSUED"]
  most_recent = true
}
