output "subnets_ids_public" {
  value = [for k, v in data.aws_subnet.fastqueue_public : data.aws_subnet.fastqueue_public[k].id]
}
