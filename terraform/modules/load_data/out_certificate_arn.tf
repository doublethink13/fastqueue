output "certificate_arn" {
  value = length(data.aws_acm_certificate.fastqueue) == 1 ? data.aws_acm_certificate.fastqueue[0].arn : ""
}
