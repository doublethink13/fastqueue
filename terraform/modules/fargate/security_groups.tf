resource "aws_security_group" "fastqueue_lb_http" {
  name        = "${var.app_name}-lb-http"
  description = "allow http inbound traffic"
  vpc_id      = var.vpc_fastqueue_id

  ingress {
    description = "Allow HTTP for all"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "fastqueue_lb_https" {
  name        = "${var.app_name}-lb-https"
  description = "allow https inbound traffic"
  vpc_id      = var.vpc_fastqueue_id

  ingress {
    description = "Allow HTTPS for all"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "fastqueue_ecs" {
  name        = "${var.app_name}-ecs"
  description = "allow local vpc http, allow all out traffic"
  vpc_id      = var.vpc_fastqueue_id

  ingress {
    description = "allow local vpc http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.vpc_fastqueue_cidr]
  }

  egress {
    description = "allow all out"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "fastqueue_lb_ecs" {
  name        = "${var.app_name}-lb-ecs"
  description = "allow all communication on port 80 between resources that have this security group"
  vpc_id      = var.vpc_fastqueue_id

  ingress {
    description = "allow http in"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    self        = true
  }

  egress {
    description = "allow http out"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    self        = true
  }
}
