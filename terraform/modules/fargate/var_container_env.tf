variable "container_env" {
  type = object({
    FASTQUEUE_DB_TYPE                       = string
    FASTQUEUE_AUTHENTICATION_MODE           = string
    FASTQUEUE_BEARER_TOKEN_VALIDITY_SECONDS = string
    FASTQUEUE_BEARER_TOKEN_SIGNING_KEY      = string
    FASTQUEUE_BEARER_TOKEN_USERNAME         = string
    FASTQUEUE_BEARER_TOKEN_HASHED_PASSWORD  = string
    FASTQUEUE_REMOVE_ITEMS_TASK_SECONDS     = string
    FASTQUEUE_REMOVE_QUEUES_TASK_SECONDS    = string
  })
}
