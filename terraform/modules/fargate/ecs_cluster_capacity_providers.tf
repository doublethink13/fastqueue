resource "aws_ecs_cluster_capacity_providers" "fastqueue" {
  cluster_name = aws_ecs_cluster.fastqueue.name

  capacity_providers = ["FARGATE_SPOT"]

  default_capacity_provider_strategy {
    capacity_provider = "FARGATE_SPOT"
  }
}
