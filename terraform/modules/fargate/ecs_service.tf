resource "aws_ecs_service" "fastqueue" {
  name                               = var.app_name
  cluster                            = aws_ecs_cluster.fastqueue.id
  task_definition                    = aws_ecs_task_definition.fastqueue.arn
  desired_count                      = 1
  force_new_deployment               = true
  deployment_minimum_healthy_percent = 100
  launch_type                        = "FARGATE"
  platform_version                   = "1.4.0"

  network_configuration {
    subnets         = var.subnets_ids_private
    security_groups = [aws_security_group.fastqueue_ecs.id, aws_security_group.fastqueue_lb_ecs.id]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.fastqueue.arn
    container_name   = var.app_name
    container_port   = var.port
  }
}
