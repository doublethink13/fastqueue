output "load_balancer_address" {
  value = aws_lb.fastqueue.dns_name
}
