resource "aws_ecs_task_definition" "fastqueue" {
  family = var.app_name

  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512

  execution_role_arn = aws_iam_role.fastqueue.arn

  container_definitions = jsonencode([
    {
      name      = var.app_name
      image     = var.image_url
      cpu       = 256
      memory    = 512
      essential = true
      portMappings = [
        {
          containerPort = var.port
          hostPort      = var.port
        }
      ]
      environment = [
        {
          name  = "FASTQUEUE_DB_TYPE"
          value = var.container_env.FASTQUEUE_DB_TYPE
        },
        {
          name  = "FASTQUEUE_AUTHENTICATION_MODE"
          value = var.container_env.FASTQUEUE_AUTHENTICATION_MODE
        },
        {
          name  = "FASTQUEUE_BEARER_TOKEN_VALIDITY_SECONDS"
          value = var.container_env.FASTQUEUE_BEARER_TOKEN_VALIDITY_SECONDS
        },
        {
          name  = "FASTQUEUE_BEARER_TOKEN_SIGNING_KEY"
          value = var.container_env.FASTQUEUE_BEARER_TOKEN_SIGNING_KEY
        },
        {
          name  = "FASTQUEUE_BEARER_TOKEN_USERNAME"
          value = var.container_env.FASTQUEUE_BEARER_TOKEN_USERNAME
        },
        {
          name  = "FASTQUEUE_BEARER_TOKEN_HASHED_PASSWORD"
          value = var.container_env.FASTQUEUE_BEARER_TOKEN_HASHED_PASSWORD
        },
        {
          name  = "FASTQUEUE_REMOVE_ITEMS_TASK_SECONDS"
          value = var.container_env.FASTQUEUE_REMOVE_ITEMS_TASK_SECONDS
        },
        {
          name  = "FASTQUEUE_REMOVE_QUEUES_TASK_SECONDS"
          value = var.container_env.FASTQUEUE_REMOVE_QUEUES_TASK_SECONDS
        },
      ]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = "fastqueue-${var.env}"
          awslogs-region        = "eu-central-1"
          awslogs-create-group  = "true"
          awslogs-stream-prefix = "fastqueue"
        }

    } }
  ])
}
