resource "aws_lb" "fastqueue" {
  name               = var.app_name
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.fastqueue_lb_http.id, aws_security_group.fastqueue_lb_https.id, aws_security_group.fastqueue_lb_ecs.id]
  subnets            = var.subnets_ids_public
}

resource "aws_lb_target_group" "fastqueue" {
  name        = var.app_name
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = var.vpc_fastqueue_id

  health_check {
    matcher = "200-399"
  }
}

resource "aws_lb_listener" "fastqueue_https" {
  count = length(var.certificate_arn) > 0 ? 1 : 0

  load_balancer_arn = aws_lb.fastqueue.arn

  port            = "443"
  protocol        = "HTTPS"
  ssl_policy      = "ELBSecurityPolicy-2016-08"
  certificate_arn = var.certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.fastqueue.arn
  }
}

resource "aws_lb_listener" "fastqueue_http_env" {
  count = length(var.certificate_arn) > 0 ? 1 : 0

  load_balancer_arn = aws_lb.fastqueue.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "fastqueue_http_feature_branch" {
  count = length(var.certificate_arn) == 0 ? 1 : 0

  load_balancer_arn = aws_lb.fastqueue.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.fastqueue.arn
  }
}

resource "aws_route53_record" "fastqueue" {
  count = length(var.certificate_arn) > 0 ? 1 : 0

  zone_id = var.route_53_treuze_xyz_zone_id
  name    = var.env == "main" ? "fastqueue.treuze.xyz" : "fastqueue.${var.env}.treuze.xyz"
  type    = "A"

  alias {
    name                   = aws_lb.fastqueue.dns_name
    zone_id                = aws_lb.fastqueue.zone_id
    evaluate_target_health = true
  }
}
