resource "aws_subnet" "fastqueue_public" {
  count = length(var.fastqueue_subnet_config_public.availability_zones)

  vpc_id = aws_vpc.fastqueue.id

  cidr_block        = var.fastqueue_subnet_config_public.cidr_blocks[count.index]
  availability_zone = var.fastqueue_subnet_config_public.availability_zones[count.index]

  tags = {
    "SubnetType" = "public"
  }
}

resource "aws_subnet" "fastqueue_private" {
  count = length(var.fastqueue_subnet_config_private.availability_zones)

  vpc_id = aws_vpc.fastqueue.id

  cidr_block        = var.fastqueue_subnet_config_private.cidr_blocks[count.index]
  availability_zone = var.fastqueue_subnet_config_private.availability_zones[count.index]

  tags = {
    "SubnetType" = "private"
  }
}
