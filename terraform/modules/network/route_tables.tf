resource "aws_route_table" "fastqueue_private" {
  vpc_id = aws_vpc.fastqueue.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.fastqueue_nat.id
  }
}

resource "aws_route_table_association" "fastqueue_private" {
  for_each = { for k, v in aws_subnet.fastqueue_private : k => v }

  subnet_id      = each.value.id
  route_table_id = aws_route_table.fastqueue_private.id
}

resource "aws_route_table" "fastqueue_public" {
  vpc_id = aws_vpc.fastqueue.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.fastqueue.id
  }
}

resource "aws_route_table_association" "fastqueue_public" {
  for_each = { for k, v in aws_subnet.fastqueue_public : k => v }

  subnet_id      = each.value.id
  route_table_id = aws_route_table.fastqueue_public.id
}
