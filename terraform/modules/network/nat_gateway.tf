resource "aws_nat_gateway" "fastqueue_nat" {
  depends_on = [aws_internet_gateway.fastqueue]

  allocation_id = aws_eip.fastqueue_nat.id
  subnet_id     = aws_subnet.fastqueue_public[0].id
}
