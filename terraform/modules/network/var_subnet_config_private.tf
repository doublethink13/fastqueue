variable "fastqueue_subnet_config_public" {
  type = object({ availability_zones = list(string), cidr_blocks = list(string) })

  default = {
    "availability_zones" = ["eu-central-1a", "eu-central-1b", "eu-central-1c"],
    "cidr_blocks"        = ["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24"]
  }

  validation {
    condition     = length(var.fastqueue_subnet_config_public.availability_zones) == length(var.fastqueue_subnet_config_public.cidr_blocks)
    error_message = "number of availability zones must match number of cidr_blocks"
  }
}
