from typing import Generator

import pytest

from scripts.setup_env_vars import main as setup_env_vars


@pytest.fixture(scope="function")
def monkeypatch() -> Generator[pytest.MonkeyPatch, None, None]:
    with pytest.MonkeyPatch.context() as monkeypatch:
        monkeypatch.delenv("CI_PROJECT_DIR", raising=False)
        monkeypatch.delenv("CI_COMMIT_BRANCH", raising=False)

        yield monkeypatch


class TestSetupEnvVars:
    mock_project_dir = "mock_project_dir"

    @pytest.mark.parametrize(
        "project_dir,commit_branch,expected_error_message",
        [
            (None, None, "CI_PROJECT_DIR env var is not available"),
            (mock_project_dir, None, "CI_COMMIT_BRANCH env var is not available"),
        ],
    )
    def test_env_vars_not_available(
        self,
        monkeypatch: pytest.MonkeyPatch,
        project_dir: str | None,
        commit_branch: str | None,
        expected_error_message: str,
    ) -> None:
        self._set_env_vars(
            monkeypatch, project_dir=project_dir, commit_branch=commit_branch
        )

        with pytest.raises(ValueError, match=expected_error_message):
            setup_env_vars()

    @pytest.mark.parametrize(
        "commit_branch,expected_full_work_dir",
        [
            (
                "mock",
                f"{mock_project_dir}/terraform/envs/feature/mock",
            ),
            (
                "dev",
                f"{mock_project_dir}/terraform/envs/dev",
            ),
        ],
    )
    def test_commit_branch(
        self,
        monkeypatch: pytest.MonkeyPatch,
        commit_branch: str,
        expected_full_work_dir: str,
        capsys: pytest.CaptureFixture[str],
    ) -> None:
        self._set_env_vars(
            monkeypatch, project_dir=self.mock_project_dir, commit_branch=commit_branch
        )

        setup_env_vars()

        assert (
            capsys.readouterr().out
            == f"export FULL_WORK_DIR={expected_full_work_dir}\n"
        )

    def _set_env_vars(
        self,
        monkeypatch: pytest.MonkeyPatch,
        project_dir: str | None = None,
        commit_branch: str | None = None,
    ) -> None:
        if project_dir:
            monkeypatch.setenv("CI_PROJECT_DIR", self.mock_project_dir)

        if commit_branch:
            monkeypatch.setenv("CI_COMMIT_BRANCH", commit_branch)
