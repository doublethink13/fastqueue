from typing import Generator
from unittest.mock import Mock, mock_open, patch

import pytest

from scripts.update_tfvars_from_env import main as update_tfvars_from_env


@pytest.fixture(scope="function")
def monkeypatch() -> Generator[pytest.MonkeyPatch, None, None]:
    with pytest.MonkeyPatch.context() as monkeypatch:
        monkeypatch.delenv("CI_REGISTRY_IMAGE", raising=False)
        monkeypatch.delenv("CI_COMMIT_BRANCH", raising=False)

        yield monkeypatch


@pytest.fixture(scope="function")
def mock_dump_function() -> Generator[Mock, None, None]:
    with patch("scripts.update_tfvars_from_env.json") as mock_json:
        mock_dump = Mock()

        mock_json.dump = mock_dump

        yield mock_dump


@pytest.fixture(scope="function")
def mock_open_function() -> Generator[Mock, None, None]:
    with patch("builtins.open", mock_open()) as mock:
        yield mock


class TestUpdateTfvarsFromEnv:
    mock_registry = "mock_registry"
    mock_branch = "mockbranch"

    def test_commit_branch_not_available(self, monkeypatch: pytest.MonkeyPatch) -> None:
        with pytest.raises(ValueError, match="CI_COMMIT_BRANCH is not present in env"):
            update_tfvars_from_env()

    def test_commit_branch_available_json_dump_is_called_with_expected_values(
        self,
        monkeypatch: pytest.MonkeyPatch,
        mock_dump_function: Mock,
        mock_open_function: Mock,
    ) -> None:
        self._set_env_vars(monkeypatch, registry_image=True, branch=True)

        update_tfvars_from_env()

        mock_dump_function.assert_called_once_with(
            {
                "env": self.mock_branch,
                "image_url": f"{self.mock_registry}:{self.mock_branch}",
            },
            mock_open_function.return_value,
            ensure_ascii=False,
            indent=4,
        )

    def _set_env_vars(
        self,
        monkeypatch: pytest.MonkeyPatch,
        registry_image: bool = False,
        branch: bool = False,
    ) -> None:
        if registry_image:
            monkeypatch.setenv("CI_REGISTRY_IMAGE", self.mock_registry)

        if branch:
            monkeypatch.setenv("CI_COMMIT_BRANCH", self.mock_branch)
