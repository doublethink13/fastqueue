import string
from datetime import datetime

import pytest
from cryptography.fernet import Fernet

from scripts.generate_bearer_token import main as generate_bearer_token


class TestGenerateBearerToken:
    fernet = Fernet("Noor0ptm0J2X2FJaJcQFkfH7JsiZYP2drSjvGSBQM_k=")
    characters = list(string.ascii_letters + string.digits)

    def test_generate_bearer_token(
        self,
        capsys: pytest.CaptureFixture[str],
    ) -> None:
        generate_bearer_token()
        encrypted_bearer_token = capsys.readouterr().out.strip()

        decrypted_bearer_token = self.fernet.decrypt(encrypted_bearer_token).decode()
        parsed_bearer_token = decrypted_bearer_token.split(":")

        assert parsed_bearer_token[0] == "fastqueue"

        datetime.fromtimestamp(float(parsed_bearer_token[1]))

        for character in parsed_bearer_token[2]:
            assert character in self.characters
