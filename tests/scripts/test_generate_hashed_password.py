import pytest
from passlib.context import CryptContext

from scripts.generate_hashed_password import main as generate_hashed_password


class TestGenerateHashedPassword:
    pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

    def test_generate_hashed_password(
        self,
        capsys: pytest.CaptureFixture[str],
    ) -> None:
        generate_hashed_password()

        hashed_password = capsys.readouterr().out.strip()

        assert self.pwd_context.verify("averysecretpassword", hashed_password) is True
