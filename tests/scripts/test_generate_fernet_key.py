import pytest
from cryptography.fernet import Fernet

from scripts.generate_fernet_key import main as generate_fernet_key


class TestGenerateFernetKey:
    def test_generate_fernet_key(
        self,
        capsys: pytest.CaptureFixture[str],
    ) -> None:
        generate_fernet_key()

        fernet_key = capsys.readouterr().out.strip()

        Fernet(fernet_key)
