import random

import pytest


@pytest.fixture(scope="session")
def fix_random_seed():
    random.seed(0)
