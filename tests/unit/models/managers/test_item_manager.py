from datetime import datetime
from typing import Generator
from unittest.mock import Mock, patch
from uuid import UUID, uuid4

import pytest

from app.models.exceptions.no_queue_found import NoQueueFoundException
from app.models.items.item import Item
from app.models.managers.item_manager import ItemManager
from app.models.payloads.create_item import CreateItemPayload

_mock_item = Item(
    name="mock", description="mock", expiry_date=datetime.now(), data="mock"
)

_number_of_items = 1

_mock_uuid = uuid4()


class MockQueue:
    queue_id = _mock_uuid
    add_item = Mock()
    get_items = Mock(return_value=[_mock_item])

    _size = _number_of_items

    def size(self) -> int:
        return self._size

    def new_size(self, value: int) -> None:
        self._size = value


_test_setup_type = tuple[Mock, Mock, ItemManager, MockQueue]


@pytest.fixture(autouse=True)
def test_setup() -> Generator[_test_setup_type, None, None]:
    with patch("app.models.managers.item_manager.QueueManager") as mock_queue_manager:
        mock_queue = MockQueue()

        mock_get_one = Mock(return_value=mock_queue)
        mock_queue_manager.return_value.get_one = mock_get_one

        yield mock_get_one, mock_queue_manager, ItemManager(), mock_queue


class TestItemManagerAddItemToQueue:
    mock_create_item_payload = CreateItemPayload(
        name="mock", description="mock", expiry_date=datetime.now(), data="mock"
    )

    def test_queue_manager_gets_one_queue_with_expected_value(
        self, test_setup: _test_setup_type
    ) -> None:
        mock_get_one, _, item_manager, _ = test_setup

        item_manager.add_item_to_queue(_mock_uuid, self.mock_create_item_payload)

        mock_get_one.assert_called_once_with(_mock_uuid)

    def test_is_added_correctly_to_queue(self, test_setup: _test_setup_type) -> None:
        _, _, item_manager, mock_queue = test_setup

        item_manager.add_item_to_queue(_mock_uuid, self.mock_create_item_payload)

        added_item = mock_queue.add_item.call_args[0][0]

        self.verify_item(added_item, self.mock_create_item_payload)

    def test_created_item_returns_expected_values(
        self, test_setup: _test_setup_type
    ) -> None:
        _, _, item_manager, _ = test_setup

        item, exception = item_manager.add_item_to_queue(
            _mock_uuid, self.mock_create_item_payload
        )

        assert exception is None

        self.verify_item(item, self.mock_create_item_payload)

    def test_handles_missing_queue(self, test_setup: _test_setup_type) -> None:
        mock_get_one, _, item_manager, _ = test_setup

        mock_get_one.configure_mock(return_value=None)

        item, exception = item_manager.add_item_to_queue(
            _mock_uuid, self.mock_create_item_payload
        )

        assert item is None

        assert isinstance(exception, NoQueueFoundException)
        assert exception.message == f"queue {_mock_uuid} not found"

    def verify_item(self, item: Item | None, payload: CreateItemPayload) -> None:
        assert item is not None
        assert item.item_id == UUID(str(item.item_id), version=4)
        assert item.name == payload.name
        assert item.description == payload.description
        assert item.expiry_date == payload.expiry_date
        assert item.data == payload.data


class TestItemManagerGetItemsFromQueue:
    @pytest.fixture(autouse=True)
    def run(self, test_setup: _test_setup_type) -> None:
        _, _, item_manager, mock_queue = test_setup

        mock_queue.get_items.reset_mock()

        self._run_test_function_and_save_return_values(item_manager)

    def _run_test_function_and_save_return_values(
        self, item_manager: ItemManager
    ) -> None:
        self.result, self.exception = item_manager.get_items_from_queue(
            _mock_uuid, _number_of_items
        )

    def test_queue_manager_gets_one_queue_with_expected_value(
        self, test_setup: _test_setup_type
    ) -> None:
        mock_get_one, _, _, _ = test_setup

        mock_get_one.assert_called_once_with(_mock_uuid)

    def test_gets_the_expected_number_of_items_from_queue(
        self, test_setup: _test_setup_type
    ) -> None:
        _, _, _, mock_queue = test_setup

        assert mock_queue.get_items.call_count == _number_of_items

    def test_returns_expected_values(self) -> None:
        assert self.exception is None
        assert self.result == [_mock_item]

    def test_handles_missing_queue(self, test_setup: _test_setup_type) -> None:
        mock_get_one, _, item_manager, _ = test_setup

        mock_get_one.configure_mock(return_value=None)

        self._run_test_function_and_save_return_values(item_manager)

        assert self.result == []

        assert isinstance(self.exception, NoQueueFoundException)
        assert self.exception.message == f"queue {_mock_uuid} not found"

    def test_handles_no_items_in_queue(self, test_setup: _test_setup_type) -> None:
        _, _, item_manager, mock_queue = test_setup

        mock_queue.get_items.configure_mock(return_value=[])

        self._run_test_function_and_save_return_values(item_manager)

        assert self.result == []
        assert self.exception is None
