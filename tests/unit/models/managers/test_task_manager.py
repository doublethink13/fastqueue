import threading
import time
from typing import Generator
from unittest.mock import Mock, patch

import pytest

from app.models.managers.task_manager import TaskManager


class MockSettings:
    remove_items_task_seconds = 1
    remove_queues_task_seconds = 1


class MockQueueManager:
    remove_expired_items_from_all_queues = Mock()
    remove_expired_queues = Mock()


class TestTaskManager:
    @pytest.fixture(autouse=True)
    def settings(self) -> Generator[None, None, None]:
        with patch("app.models.managers.task_manager.Settings") as mock_settings:
            mock_settings_singleton = MockSettings()
            mock_settings.singleton = Mock(return_value=mock_settings_singleton)

            yield

    @pytest.fixture()
    def queue_manager(self) -> Generator[MockQueueManager, None, None]:
        with patch(
            "app.models.managers.task_manager.QueueManager"
        ) as mock_queue_manager:
            queue_manager = MockQueueManager()
            mock_queue_manager.return_value = queue_manager

            yield queue_manager

    def test_remove_all_items(self, queue_manager: MockQueueManager) -> None:
        threading.Thread(
            target=TaskManager.task_running_startup, args=(), daemon=True
        ).start()

        time.sleep(2)

        queue_manager.remove_expired_items_from_all_queues.assert_called_once()
        queue_manager.remove_expired_queues.assert_called_once()
