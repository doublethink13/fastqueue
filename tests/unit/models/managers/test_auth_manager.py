from typing import Any, Generator
from unittest.mock import Mock, patch

import pytest

from app.models.enums.authentication_mode import AuthenticationMode
from app.models.enums.bearer_token_failed_validation import BearerTokenFailedValidation
from app.models.enums.db_type import DbType
from app.models.exceptions.auth_settings import AuthSettingsException
from app.models.managers.auth_manager import AuthManager


class MockSettings:
    authentication_mode: AuthenticationMode = AuthenticationMode.Off

    bearer_token_validity_seconds: int = 600
    bearer_token_signing_key: str = "Noor0ptm0J2X2FJaJcQFkfH7JsiZYP2drSjvGSBQM_k="
    bearer_token_username: str = "fastqueue"
    bearer_token_hashed_password: str = "mockhashedpassword"

    db_type: DbType = DbType.Memory

    def __setitem__(self, key: str, value: Any):
        self.__dict__[key] = value


@pytest.fixture(autouse=True)
def settings_singleton() -> Generator[MockSettings, None, None]:
    with patch("app.models.managers.auth_manager.Settings") as mock_settings:
        mock_settings_singleton = MockSettings()
        mock_settings.singleton = Mock(return_value=mock_settings_singleton)

        yield mock_settings_singleton


class TestAuthManager:
    def test_singleton(self) -> None:
        auth_manager_1 = AuthManager.singleton()
        auth_manager_2 = AuthManager.singleton()

        assert id(auth_manager_1) == id(auth_manager_2)

    @pytest.mark.parametrize(
        "disabled_property,expected_error_message",
        [
            (
                "bearer_token_signing_key",
                (
                    "Auth settings are not valid: "
                    + "{'authentication_mode': 'bearer', "
                    + "'bearer_token_validity_seconds': 600, "
                    + "'bearer_token_signing_key': '', "
                    + "'bearer_token_username': 'fastqueue', "
                    + "'bearer_token_hashed_password': 'mockhashedpassword'}"
                ),
            ),
            (
                "bearer_token_username",
                (
                    "Auth settings are not valid: "
                    + "{'authentication_mode': 'bearer', "
                    + "'bearer_token_validity_seconds': 600, "
                    + "'bearer_token_signing_key': "
                    + "'Noor0ptm0J2X2FJaJcQFkfH7JsiZYP2drSjvGSBQM_k=', "
                    + "'bearer_token_username': '', "
                    + "'bearer_token_hashed_password': 'mockhashedpassword'}"
                ),
            ),
            (
                "bearer_token_hashed_password",
                (
                    "Auth settings are not valid: "
                    + "{'authentication_mode': 'bearer', "
                    + "'bearer_token_validity_seconds': 600, "
                    + "'bearer_token_signing_key': "
                    + "'Noor0ptm0J2X2FJaJcQFkfH7JsiZYP2drSjvGSBQM_k=', "
                    + "'bearer_token_username': 'fastqueue', "
                    + "'bearer_token_hashed_password': ''}"
                ),
            ),
        ],
    )
    def test_raises_error_init_if_auth_settings_are_invalid_missing_configs(
        self,
        settings_singleton: MockSettings,
        disabled_property: str,
        expected_error_message: str,
    ) -> None:
        settings_singleton.authentication_mode = AuthenticationMode.Bearer
        settings_singleton[disabled_property] = ""

        with pytest.raises(AuthSettingsException, match=expected_error_message):
            AuthManager()

    @pytest.mark.parametrize(
        "authentication_mode,expected_result",
        [(AuthenticationMode.Bearer, True), (AuthenticationMode.Off, False)],
    )
    def test_is_auth_active(
        self,
        settings_singleton: MockSettings,
        authentication_mode: AuthenticationMode,
        expected_result: bool,
    ) -> None:
        settings_singleton.authentication_mode = authentication_mode

        auth_manager = AuthManager()

        assert auth_manager.is_auth_active() == expected_result

    @pytest.mark.parametrize(
        "settings_username,test_username,expected_result",
        [("imokay", "imokay", True), ("imokay", "imnotokay", False)],
    )
    def test_validate_username(
        self,
        settings_singleton: MockSettings,
        settings_username: str,
        test_username: str,
        expected_result: bool,
    ) -> None:
        settings_singleton.bearer_token_username = settings_username

        auth_manager = AuthManager()

        assert auth_manager.validate_username(test_username) == expected_result

    @pytest.mark.parametrize(
        "settings_hashed_password,test_password,expected_result",
        [
            (
                "$2b$12$PrNaZqMqTCRg1AERWmUgruSpMbQbb/9WdkzAICLSLZSkjHgamnD1C",
                "imokay",
                True,
            ),
            (
                "$2b$12$PrNaZqMqTCRg1AERWmUgruSpMbQbb/9WdkzAICLSLZSkjHgamnD1C",
                "imnotokay",
                False,
            ),
        ],
    )
    def test_validate_password(
        self,
        settings_singleton: MockSettings,
        settings_hashed_password: str,
        test_password: str,
        expected_result: bool,
    ) -> None:
        settings_singleton.bearer_token_hashed_password = settings_hashed_password

        auth_manager = AuthManager()

        assert auth_manager.validate_password(test_password) == expected_result

    @pytest.mark.parametrize(
        "bearer_token,expected_result",
        [
            (
                "gAAAAABjkhOvlb9w5Y22sGuOhvoeC"
                + "jqJVYt6kiGJEnCKzfABJ5bH3QDH"
                + "nynMsIf25TMze6_jHmqzYS8FQHT"
                + "BDq_N27RX93Czvd_QF1P4xGOF4O"
                + "TsUvdTFz7dGTV5cN0g2BI2ORuREsEg",
                None,
            ),
            (
                "gAAAAABjkhPl0eFW0ppA56UvPAttp"
                + "Z5gUu5MdMKlPJhWbCkCDSht6yi7"
                + "Qpyj2KcRf47T1dtVsofK8BW4VKa"
                + "82awH9R8lSlLRolW2LbUX-x1BGw"
                + "onrNvetchxSWeByERNQwmBIKrww7hM",
                BearerTokenFailedValidation.Expired,
            ),
            (
                "gAAAAABjkhQ9mCdZpMB43Q6BRVbA0"
                + "-wB6xD6byys0GtWIwSdaqcsjwmC"
                + "X2SkbbaBe8rNzv5KQ8C8pXZx9_m"
                + "e-5yYZEGrGfw4J9BWpzphcZWMC4"
                + "w16zY4lC6U0t5kmixyZP2x7wKlFylG",
                BearerTokenFailedValidation.InvalidUsername,
            ),
            (
                "gAAAAABjkhQVrXtdB3OHBNKuPtQyX"
                + "rSpNN7wHcJalpA3QRx7GTstCoQV"
                + "g98IpdJC2VpPodPp3LfC0c_OfWL"
                + "-s5bsgVAwYUh3V8lpjoIV4YM30B"
                + "iRWkPADdo7gyksW-8DNpsZEt-3uror",
                BearerTokenFailedValidation.InvalidUsername,
            ),
            ("123", BearerTokenFailedValidation.InvalidFormat),
        ],
    )
    def test_validate_bearer_token(
        self,
        bearer_token: str,
        expected_result: BearerTokenFailedValidation | None,
    ) -> None:
        auth_manager = AuthManager()

        assert auth_manager.validate_bearer_token(bearer_token) == expected_result

    def test_generate_bearer_token(self) -> None:
        auth_manager = AuthManager()

        bearer_token = auth_manager.generate_bearer_token("fastqueue")

        assert auth_manager.validate_bearer_token(bearer_token) is None
