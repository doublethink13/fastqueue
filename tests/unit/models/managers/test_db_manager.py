from datetime import datetime
from typing import Generator
from unittest.mock import Mock, patch
from uuid import UUID, uuid4

import pytest

from app.models.managers.db_manager import DbManager
from app.models.queues.random import RandomQueue

_mock_return = Mock()


class MockDb:
    delete_connection = Mock()
    save_queue = Mock(return_value=_mock_return)
    get_queue = Mock(return_value=_mock_return)
    get_all_queues = Mock(return_value=[_mock_return])
    delete_queue = Mock(return_value=_mock_return)


_test_setup_type = tuple[MockDb, Mock, DbManager, RandomQueue, UUID]


@pytest.fixture(autouse=True)
def test_setup() -> Generator[_test_setup_type, None, None]:
    with patch("app.models.managers.db_manager.InMemoryDb") as mock_in_memory_db:
        mock_db = MockDb()

        mock_singleton = Mock(return_value=mock_db)
        mock_in_memory_db.singleton = mock_singleton

        db_manager = DbManager()

        mock_queue = RandomQueue(
            name="mock",
            description="mock",
            expiry_date=datetime.now(),
        )

        mock_uuid = uuid4()

        yield mock_db, mock_singleton, db_manager, mock_queue, mock_uuid


class TestDbManager:
    def test_get_db_manager(self, test_setup: _test_setup_type) -> None:
        _, mock_singleton, _, _, _ = test_setup

        DbManager.get_db_manager()

        mock_singleton.assert_called_once()

    def test_delete_connection(self, test_setup: _test_setup_type) -> None:
        mock_db, _, db_manager, _, _ = test_setup

        db_manager.setup_db()

        db_manager.delete_db_connection()

        mock_db.delete_connection.assert_called_once()


class TestDbManagerSaveQueue:
    def test_save_queue_with_expected_value(self, test_setup: _test_setup_type) -> None:
        mock_db, _, db_manager, mock_queue, _ = test_setup

        db_manager.save_queue(mock_queue)

        mock_db.save_queue.assert_called_once_with(mock_queue)

    def test_save_queue_returns_expected_value(
        self, test_setup: _test_setup_type
    ) -> None:
        _, _, db_manager, mock_queue, _ = test_setup

        result = db_manager.save_queue(mock_queue)

        assert result == _mock_return


class TestDbManagerGetQueue:
    def test_get_queue_with_expected_value(self, test_setup: _test_setup_type) -> None:
        mock_db, _, db_manager, _, mock_uuid = test_setup

        db_manager.get_queue(mock_uuid)

        mock_db.get_queue.assert_called_once_with(mock_uuid)

    def test_get_queue_returns_expected_value(
        self, test_setup: _test_setup_type
    ) -> None:
        _, _, db_manager, _, mock_uuid = test_setup

        result = db_manager.get_queue(mock_uuid)

        assert result == _mock_return


class TestDbManagerGetAllQueues:
    def test_get_all_queues_with_expected_value(
        self, test_setup: _test_setup_type
    ) -> None:
        mock_db, _, db_manager, _, _ = test_setup

        db_manager.get_all_queues()

        mock_db.get_all_queues.assert_called_once()

    def test_get_all_queues_returns_expected_value(
        self, test_setup: _test_setup_type
    ) -> None:
        _, _, db_manager, _, _ = test_setup

        result = db_manager.get_all_queues()

        assert result == [_mock_return]


class TestDbManagerDeleteQueue:
    def test_delete_queue_with_expected_value(
        self, test_setup: _test_setup_type
    ) -> None:
        mock_db, _, db_manager, _, mock_uuid = test_setup

        db_manager.delete_queue(mock_uuid)

        mock_db.delete_queue.assert_called_once_with(mock_uuid)

    def test_delete_queue_returns_expected_value(
        self, test_setup: _test_setup_type
    ) -> None:
        _, _, db_manager, _, mock_uuid = test_setup

        result = db_manager.delete_queue(mock_uuid)

        assert result == _mock_return
