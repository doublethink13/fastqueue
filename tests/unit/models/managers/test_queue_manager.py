from datetime import datetime
from typing import Generator
from unittest.mock import Mock, patch
from uuid import uuid4

import pytest

from app.models.enums.queue_type import QueueType
from app.models.items.item import Item
from app.models.managers.queue_manager import QueueManager
from app.models.payloads.create_queue import CreateQueuePayload
from app.models.queues.random import RandomQueue

_test_setup_type = tuple[RandomQueue, Mock, Mock, Mock, Mock, Mock]


@pytest.fixture(autouse=True)
def test_setup() -> Generator[_test_setup_type, None, None]:
    with patch("app.models.managers.queue_manager.QueueFactory") as mock_queue_factory:
        with patch("app.models.managers.queue_manager.DbManager") as mock_db_manager:
            mock_item = Item(
                name="mockitem",
                description="mockitem",
                expiry_date=datetime.now(),
                data=[],
            )

            mock_queue = RandomQueue(
                name="mockqueue",
                description="mockqueue",
                expiry_date=datetime.now(),
            )

            mock_queue.add_item(mock_item)

            mock_create_queue_queue_factory = Mock(return_value=mock_queue)
            mock_queue_factory.return_value.create_queue = (
                mock_create_queue_queue_factory
            )

            mock_save_queue_db_manager = Mock()
            mock_get_queue = Mock()
            mock_get_all_queues = Mock(return_value=[mock_queue])
            mock_delete_queue = Mock()

            mock_db_manager.return_value.save_queue = mock_save_queue_db_manager
            mock_db_manager.return_value.get_queue = mock_get_queue
            mock_db_manager.return_value.get_all_queues = mock_get_all_queues
            mock_db_manager.return_value.delete_queue = mock_delete_queue

            yield (
                mock_queue,
                mock_create_queue_queue_factory,
                mock_save_queue_db_manager,
                mock_get_queue,
                mock_get_all_queues,
                mock_delete_queue,
            )


class TestQueueManager:
    mock_uuid = uuid4()

    @pytest.fixture(autouse=True)
    def queue_manager(self) -> Generator[QueueManager, None, None]:
        yield QueueManager()

    def test_create(
        self, test_setup: _test_setup_type, queue_manager: QueueManager
    ) -> None:
        (
            mock_queue,
            mock_create_queue_queue_factory,
            mock_save_queue_db_manager,
            _,
            _,
            _,
        ) = test_setup

        mock_create_queue_db_manager_payload = CreateQueuePayload(
            name="mock",
            description="mock",
            expiry_date=datetime.now(),
            queue_type=QueueType.Random,
        )

        queue_manager.create(mock_create_queue_db_manager_payload)

        mock_create_queue_queue_factory.assert_called_once_with(
            mock_create_queue_db_manager_payload
        )
        mock_save_queue_db_manager.assert_called_once_with(mock_queue)

    def test_get_one(
        self, test_setup: _test_setup_type, queue_manager: QueueManager
    ) -> None:
        _, _, _, mock_get_queue, _, _ = test_setup

        queue_manager.get_one(self.mock_uuid)

        mock_get_queue.assert_called_once_with(self.mock_uuid)

    def test_get_all_queues(
        self, test_setup: _test_setup_type, queue_manager: QueueManager
    ) -> None:
        _, _, _, _, mock_get_all_queues, _ = test_setup

        queue_manager.get_all_queues()

        mock_get_all_queues.assert_called_once()

    def test_delete_one(
        self, test_setup: _test_setup_type, queue_manager: QueueManager
    ) -> None:
        _, _, _, _, _, mock_delete_queue = test_setup

        queue_manager.delete_one(self.mock_uuid)

        mock_delete_queue.assert_called_once_with(self.mock_uuid)

    def test_remove_expired_items_from_all_queues(
        self, test_setup: _test_setup_type, queue_manager: QueueManager
    ) -> None:
        mock_queue, _, _, _, _, _ = test_setup

        assert mock_queue.size() == 1

        queue_manager.remove_expired_items_from_all_queues()

        assert mock_queue.size() == 0

    def test_remove_expired_queues(
        self, test_setup: _test_setup_type, queue_manager: QueueManager
    ) -> None:
        _, _, _, _, _, mock_delete_queue = test_setup

        queue_manager.remove_expired_queues()

        mock_delete_queue.assert_called_once()
