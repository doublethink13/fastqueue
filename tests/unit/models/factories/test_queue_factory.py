from datetime import datetime
from uuid import UUID

from app.models.enums.queue_type import QueueType
from app.models.factories.queue_factory import QueueFactory
from app.models.payloads.create_queue import CreateQueuePayload


class TestQueueFactory:
    queue_factory = QueueFactory()

    def test_create_lifo_queue(self) -> None:
        self.create_and_verify_queue(QueueType.LIFO)

    def test_create_fifo_queue(self) -> None:
        self.create_and_verify_queue(QueueType.FIFO)

    def test_create_random_queue(self) -> None:
        self.create_and_verify_queue(QueueType.Random)

    def create_and_verify_queue(self, queue_type: QueueType) -> None:
        mock_payload = CreateQueuePayload(
            name="mock",
            description="mock",
            expiry_date=datetime.now(),
            queue_type=queue_type,
        )

        queue = self.queue_factory.create_queue(mock_payload)

        assert queue.queue_id == UUID(str(queue.queue_id), version=4)
        assert queue.description == mock_payload.description
        assert queue.expiry_date == mock_payload.expiry_date
        assert queue.name == mock_payload.name
        assert queue.queue_type == mock_payload.queue_type
        assert len(queue.items) == 0
