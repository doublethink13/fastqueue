from datetime import datetime
from typing import Generator

import pytest

from app.models.dbs.in_memory import InMemoryDb
from app.models.dbs.interface import DbInterface
from app.models.queues.random import RandomQueue

_test_setup_type = tuple[RandomQueue, DbInterface]


class TestInMemoryDb:
    @pytest.fixture(autouse=True)
    def test_setup(self) -> Generator[_test_setup_type, None, None]:
        db = InMemoryDb()

        mock_queue = RandomQueue(
            name="mock", description="mock", expiry_date=datetime.now()
        )

        db.save_queue(mock_queue)

        yield mock_queue, db

    def test_singleton(self):
        db1 = InMemoryDb.singleton()
        db2 = InMemoryDb.singleton()

        assert id(db1) == id(db2)

    def test_delete_connection(self) -> None:
        db1 = InMemoryDb.singleton()
        db_id_1 = id(db1)

        db1.delete_connection()

        db_id_2 = id(InMemoryDb.singleton())

        assert db_id_1 != db_id_2

    def test_get_queue(self, test_setup: _test_setup_type) -> None:
        mock_queue, mock_db = test_setup

        assert mock_db.get_queue(mock_queue.queue_id) == mock_queue

    def test_get_all_queues(self, test_setup: _test_setup_type) -> None:
        mock_queue, mock_db = test_setup

        assert mock_db.get_all_queues() == [mock_queue]

    def test_delete_queue(self, test_setup: _test_setup_type) -> None:
        mock_queue, mock_db = test_setup

        assert mock_db.delete_queue(mock_queue.queue_id) == mock_queue
