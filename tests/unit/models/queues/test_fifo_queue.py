from datetime import datetime

import pytest

from app.models.items.item import Item
from app.models.queues.fifo import FifoQueue


class TestFifoQueue:
    @pytest.fixture(autouse=True, scope="function")
    def queue(self) -> FifoQueue:
        queue = FifoQueue(name="mock", description="mock", expiry_date=datetime.now())

        self._add_items_to_queue(queue)

        return queue

    def _add_items_to_queue(self, queue: FifoQueue) -> None:
        for i in range(3):
            queue.add_item(
                Item(
                    name=f"item-{i}",
                    description="mock",
                    expiry_date=datetime.now(),
                    data=i,
                )
            )

    def test_get_one_item(self, queue: FifoQueue) -> None:
        self._assert_items(queue, 1)

    def test_get_two_items(self, queue: FifoQueue) -> None:
        self._assert_items(queue, 2)

    def test_get_three_items(self, queue: FifoQueue) -> None:
        self._assert_items(queue, 3)

    def test_get_four_items(self, queue: FifoQueue) -> None:
        self._assert_items(queue, 3)

        assert queue.get_items() == []

    def _assert_items(self, queue: FifoQueue, number_of_items: int) -> None:
        items = queue.get_items(number_of_items)

        assert len(items) == number_of_items

        for index, item in enumerate(items):
            assert item.name == f"item-{index}"
