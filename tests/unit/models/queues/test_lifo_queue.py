from datetime import datetime

import pytest

from app.models.items.item import Item
from app.models.queues.lifo import LifoQueue


class TestLifoQueue:
    @pytest.fixture(autouse=True, scope="function")
    def queue(self) -> LifoQueue:
        queue = LifoQueue(name="mock", description="mock", expiry_date=datetime.now())

        self._add_items_to_queue(queue)

        return queue

    def _add_items_to_queue(self, queue: LifoQueue) -> None:
        for i in range(3):
            queue.add_item(
                Item(
                    name=f"item-{i}",
                    description="mock",
                    expiry_date=datetime.now(),
                    data=i,
                )
            )

    def test_get_one_item(self, queue: LifoQueue) -> None:
        self._assert_items(queue, 1, 2, 0)

    def test_get_two_items(self, queue: LifoQueue) -> None:
        self._assert_items(queue, 2, 2, 0)

    def test_get_three_items(self, queue: LifoQueue) -> None:
        self._assert_items(queue, 3, 2, -1)

    def test_get_four_items(self, queue: LifoQueue) -> None:
        self._assert_items(queue, 3, 2, -1)

        assert queue.get_items() == []

    def _assert_items(
        self, queue: LifoQueue, number_of_items: int, start: int, end: int
    ) -> None:
        items = queue.get_items(number_of_items)

        expected_order = list(range(start, end, -1))

        for index, item in enumerate(items):
            assert item.name == f"item-{expected_order[index]}"
