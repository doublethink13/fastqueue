from datetime import datetime

import pytest

from app.models.items.item import Item
from app.models.queues.random import RandomQueue


class TestRandomQueue:
    @pytest.fixture(autouse=True, scope="function")
    def queue(self) -> RandomQueue:
        queue = RandomQueue(name="mock", description="mock", expiry_date=datetime.now())

        self._add_items_to_queue(queue)

        return queue

    def _add_items_to_queue(self, queue: RandomQueue) -> None:
        for i in range(3):
            queue.add_item(
                Item(
                    name=f"item-{i}",
                    description="mock",
                    expiry_date=datetime.now(),
                    data=i,
                )
            )

    def test_get_one_item(self, queue: RandomQueue) -> None:
        number_of_items = 1

        self._assert_items(queue.get_items(number_of_items), number_of_items)

    def test_get_two_items(self, queue: RandomQueue) -> None:
        number_of_items = 2

        self._assert_items(queue.get_items(number_of_items), number_of_items)

    def test_get_three_items(self, queue: RandomQueue) -> None:
        number_of_items = 3

        self._assert_items(queue.get_items(number_of_items), number_of_items)

    def test_get_four_items(self, queue: RandomQueue) -> None:
        number_of_items = 3

        self._assert_items(queue.get_items(number_of_items), number_of_items)

        assert queue.get_items() == []

    def _assert_items(self, items: list[Item], number_of_items: int) -> None:
        assert len(items) == number_of_items

        expected_items = ["item-0", "item-1", "item-2"]

        for item in items:
            assert item.name in expected_items
