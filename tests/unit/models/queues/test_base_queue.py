from datetime import datetime
from typing import Generator

import pytest

from app.models.items.item import Item
from app.models.queues.random import RandomQueue


class TestBaseQueue:
    @pytest.fixture()
    def queue(self) -> Generator[RandomQueue, None, None]:
        mock_queue = RandomQueue(
            name="mockqueue",
            description="mockqueue",
            expiry_date=None,
        )

        mock_queue.add_item(
            Item(
                name="mockitem",
                description="mockitem",
                expiry_date=datetime.fromtimestamp(1),
                data=[],
            )
        )

        yield mock_queue

    def test_size(self, queue: RandomQueue) -> None:
        assert queue.size() == 1

    def test_remove_expired_items(self, queue: RandomQueue) -> None:
        queue.remove_expired_items()

        assert queue.size() == 0
