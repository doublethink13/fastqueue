from unittest.mock import patch

import pytest
from fastapi.exceptions import HTTPException

from app.handlers.authorization.authorize import authorize
from app.models.enums.bearer_token_failed_validation import BearerTokenFailedValidation
from app.models.managers.auth_manager import AuthManager


class MockAuthManager(AuthManager):
    validate_bearer_token_result: BearerTokenFailedValidation | None = None
    is_auth_active_result = True
    validate_bearer_token_error = False

    def __init__(self) -> None:
        ...

    def validate_bearer_token(
        self, bearer_token: str
    ) -> BearerTokenFailedValidation | None:
        if MockAuthManager.validate_bearer_token_error:
            raise ValueError(bearer_token)

        return MockAuthManager.validate_bearer_token_result

    def is_auth_active(self) -> bool:
        return MockAuthManager.is_auth_active_result


class TestAuthorize:
    mock_token = "mocktoken"

    @pytest.fixture(autouse=True)
    def setup(self):
        MockAuthManager.is_auth_active_result = True
        MockAuthManager.validate_bearer_token_result = None
        MockAuthManager.validate_bearer_token_error = False

    def test_authorize(self) -> None:
        authorize(self.mock_token, MockAuthManager())

    def test_auth_is_not_active(self) -> None:
        MockAuthManager.is_auth_active_result = False

        mock_auth_manager = MockAuthManager()

        with patch.object(
            mock_auth_manager, "validate_bearer_token"
        ) as mock_validate_bearer_token:
            authorize(self.mock_token, mock_auth_manager)

            mock_validate_bearer_token.assert_not_called()

    def test_raises_an_exception_if_token_is_not_valid(self) -> None:
        MockAuthManager.validate_bearer_token_result = (
            BearerTokenFailedValidation.Expired
        )

        with pytest.raises(HTTPException) as httpException:
            authorize(self.mock_token, MockAuthManager())

            assert httpException.value.status_code == 401
            assert (
                httpException.value.detail
                == f"Bearer token not valid: {BearerTokenFailedValidation.Expired}"
            )
