from unittest.mock import Mock, patch

import pytest

from app.handlers.events.startup import startup
from app.models.managers.task_manager import TaskManager


class TestStartup:
    @pytest.fixture(autouse=True)
    def run_startup(self):
        with patch("app.handlers.events.startup.Settings") as mock_settings:
            with patch("app.handlers.events.startup.DbManager") as mock_db_manager:
                with patch(
                    "app.handlers.events.startup.AuthManager"
                ) as mock_auth_manager:
                    with patch("app.handlers.events.startup.Thread") as mock_thread:
                        self.mock_settings_singleton = Mock()
                        mock_settings.singleton = self.mock_settings_singleton

                        self.mock_auth_manager_singleton = Mock()
                        mock_auth_manager.singleton = self.mock_auth_manager_singleton

                        self.mock_get_db_manager = Mock()
                        mock_db_manager.get_db_manager = self.mock_get_db_manager

                        self.mock_start = Mock()
                        self.mock_thread_instance = Mock()
                        self.mock_thread_instance.start = self.mock_start
                        self.mock_thread = mock_thread
                        mock_thread.return_value = self.mock_thread_instance

                        startup()

                        yield

    def test_settings_singleton_is_initialized(self):
        self.mock_settings_singleton.assert_called_once()

    def test_auth_manager_singleton_is_initialized(self):
        self.mock_auth_manager_singleton.assert_called_once()

    def test_in_memory_db_is_initialized(self) -> None:
        self.mock_get_db_manager.assert_called_once()

    def test_tasks_thread_is_created(self) -> None:
        self.mock_thread.assert_called_once_with(
            target=TaskManager.task_running_startup, args=(), daemon=True
        )
        self.mock_start.assert_called_once()
