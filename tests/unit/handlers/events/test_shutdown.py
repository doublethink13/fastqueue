from unittest.mock import Mock, patch

import pytest

from app.handlers.events.shutdown import shutdown


class TestShutdown:
    @pytest.fixture(autouse=True)
    def run_shutdown(self):
        with patch("app.handlers.events.shutdown.DbManager") as mock_db_manager:
            self.mock_delete_db_connection = Mock()
            mock_db_manager.return_value.delete_db_connection = (
                self.mock_delete_db_connection
            )

            shutdown()

            yield

    def test_db_connection_is_closed(self) -> None:
        self.mock_delete_db_connection.assert_called_once()
