from datetime import datetime
from http import HTTPStatus
from unittest.mock import Mock
from uuid import uuid4

import pytest
from fastapi import HTTPException

from app.handlers.item.get_items import get_items
from app.models.exceptions.no_item_in_queue import NoItemInQueueException
from app.models.exceptions.no_queue_found import NoQueueFoundException
from app.models.items.item import Item
from app.models.responses.item import ItemResponse


class TestGetOne:
    @pytest.fixture(autouse=True)
    def setup(self):
        self.mock_error_message = "ups"

        self.mock_uuid = uuid4()

        self.mock_number_of_items = 1

        self.mock_item = Item(
            name="mock", description="mock", expiry_date=datetime.now(), data="mock"
        )

        self.mock_get_items_from_queue = Mock()
        self.mock_get_items_from_queue.return_value = ([self.mock_item], None)

        self.mock_item_manager = Mock()
        self.mock_item_manager.get_items_from_queue = self.mock_get_items_from_queue

    def test_get_items_from_queue_is_called_with_expected_values(self) -> None:
        get_items(self.mock_uuid, self.mock_number_of_items, self.mock_item_manager)

        self.mock_get_items_from_queue.assert_called_once_with(
            self.mock_uuid, self.mock_number_of_items
        )

    def test_returns_expected_value(self) -> None:
        result = get_items(
            self.mock_uuid, self.mock_number_of_items, self.mock_item_manager
        )

        assert result == [
            ItemResponse(
                item_id=self.mock_item.item_id,
                name=self.mock_item.name,
                description=self.mock_item.description,
                expiry_date=self.mock_item.expiry_date,
                data=self.mock_item.data,
            )
        ]

    def test_handles_no_queue_found_exception(self) -> None:
        self.mock_get_items_from_queue.configure_mock(
            return_value=([], NoQueueFoundException(self.mock_error_message))
        )

        with pytest.raises(HTTPException) as exception:
            get_items(self.mock_uuid, self.mock_number_of_items, self.mock_item_manager)

            assert exception.value.status_code == HTTPStatus.NOT_FOUND
            assert exception.value.detail == self.mock_error_message

    def test_handles_no_items_in_queue_exception(self) -> None:
        self.mock_get_items_from_queue.configure_mock(
            return_value=([], NoItemInQueueException(self.mock_error_message))
        )

        with pytest.raises(HTTPException) as exception:
            get_items(self.mock_uuid, self.mock_number_of_items, self.mock_item_manager)

            assert exception.value.status_code == HTTPStatus.INTERNAL_SERVER_ERROR
            assert exception.value.detail == self.mock_error_message
