from datetime import datetime
from http import HTTPStatus
from unittest.mock import Mock, call
from uuid import uuid4

import pytest
from fastapi import HTTPException

from app.handlers.item.create_items import create_items
from app.models.exceptions.no_queue_found import NoQueueFoundException
from app.models.items.item import Item
from app.models.payloads.create_item import CreateItemPayload


class TestCreateItems:
    @pytest.fixture(autouse=True)
    def setup(self):
        self.mock_uuid = uuid4()

        self.mock_create_items_payload = [
            CreateItemPayload(name="mock1", description="mock1", data="mock1"),
            CreateItemPayload(name="mock2", description="mock2", data="mock2"),
            CreateItemPayload(name="mock3", description="mock3", data="mock3"),
        ]

        self.mock_item = Item(
            name="mock",
            description="mock",
            expiry_date=datetime.now(),
            data="mock",
        )

        self.mock_items = [self.mock_item, self.mock_item, self.mock_item]

        self.mock_add_item_to_queue = Mock()
        self.mock_add_item_to_queue.return_value = (self.mock_item, None)

        self.mock_item_manager = Mock()
        self.mock_item_manager.add_item_to_queue = self.mock_add_item_to_queue

    def test_add_item_to_queue_is_called_with_expected_values(self) -> None:
        create_items(
            self.mock_uuid, self.mock_create_items_payload, self.mock_item_manager
        )

        expected_calls = [
            call(self.mock_uuid, mock_create_item_payload)
            for mock_create_item_payload in self.mock_create_items_payload
        ]

        self.mock_add_item_to_queue.assert_has_calls(expected_calls)

    def test_handles_no_queue_found_exception(self):
        self.mock_add_item_to_queue.configure_mock(
            return_value=(None, NoQueueFoundException(""))
        )

        with pytest.raises(HTTPException) as exception:
            create_items(
                self.mock_uuid, self.mock_create_items_payload, self.mock_item_manager
            )

            assert exception.value.status_code == HTTPStatus.NOT_FOUND
            assert exception.value.detail == f"queue {self.mock_uuid} not found"

    def test_handles_none_item_after_queue_add(self):
        self.mock_add_item_to_queue.configure_mock(return_value=(None, None))

        created_items = create_items(
            self.mock_uuid, self.mock_create_items_payload, self.mock_item_manager
        )

        assert len(created_items) == 0

    def test_return_value(self) -> None:
        created_items_response = create_items(
            self.mock_uuid, self.mock_create_items_payload, self.mock_item_manager
        )

        assert [
            created_item_response.dict()
            for created_item_response in created_items_response
        ] == [
            {
                "data": self.mock_item.data,
                "description": self.mock_item.description,
                "expiry_date": self.mock_item.expiry_date,
                "item_id": self.mock_item.item_id,
                "name": self.mock_item.name,
            }
            for _ in range(3)
        ]
