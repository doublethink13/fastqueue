from datetime import datetime
from unittest.mock import Mock

import pytest

from app.handlers.queue.get_all_queues_details import get_all_queues_details
from app.models.queues.random import RandomQueue
from app.models.responses.queue_details import QueueDetailResponse


class TestGetAllQueuesDetails:
    @pytest.fixture(autouse=True)
    def setup(self) -> None:
        self.mock_queue = RandomQueue(
            name="mock", description="mock", expiry_date=datetime.now()
        )

        self.mock_get_all_queues = Mock(return_value=[self.mock_queue])

        self.mock_queue_manager = Mock()
        self.mock_queue_manager.get_all_queues = self.mock_get_all_queues

    def test_get_queue_is_called_with_the_expected_value(self) -> None:
        get_all_queues_details(self.mock_queue_manager)

        self.mock_get_all_queues.assert_called_once()

    def test_all_queues_details_return_value(self) -> None:
        result = get_all_queues_details(self.mock_queue_manager)

        assert result == [
            QueueDetailResponse(
                queue_id=self.mock_queue.queue_id,
                name=self.mock_queue.name,
                description=self.mock_queue.description,
                expiry_date=self.mock_queue.expiry_date,
                queue_type=self.mock_queue.queue_type,
                number_of_items=len(self.mock_queue.items),
            )
        ]
