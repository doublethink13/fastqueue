from datetime import datetime
from unittest.mock import Mock

import pytest

from app.handlers.queue.create_one import create_one_queue
from app.models.enums.queue_type import QueueType
from app.models.payloads.create_queue import CreateQueuePayload
from app.models.queues.random import RandomQueue
from app.models.responses.queue_details import QueueDetailResponse


class TestCreateOne:
    @pytest.fixture(autouse=True)
    def setup(self) -> None:
        self.mock_create_queue_payload = CreateQueuePayload(
            name="mock",
            description="mock",
            expiry_date=datetime.now(),
            queue_type=QueueType.FIFO,
        )

        self.mock_queue = RandomQueue(
            name="mock", description="mock", expiry_date=datetime.now()
        )

        self.mock_create = Mock(return_value=self.mock_queue)

        self.mock_queue_manager = Mock()
        self.mock_queue_manager.create = self.mock_create

    def test_create_called_with_expected_value(self) -> None:
        create_one_queue(self.mock_create_queue_payload, self.mock_queue_manager)

        self.mock_create.assert_called_once_with(self.mock_create_queue_payload)

    def test_create_response(self) -> None:
        result = create_one_queue(
            self.mock_create_queue_payload, self.mock_queue_manager
        )

        assert result == QueueDetailResponse(
            queue_id=self.mock_queue.queue_id,
            name=self.mock_queue.name,
            description=self.mock_queue.description,
            expiry_date=self.mock_queue.expiry_date,
            queue_type=self.mock_queue.queue_type,
            number_of_items=len(self.mock_queue.items),
        )
