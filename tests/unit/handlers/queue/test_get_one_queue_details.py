from datetime import datetime
from http import HTTPStatus
from unittest.mock import Mock
from uuid import uuid4

import pytest
from fastapi import HTTPException

from app.handlers.queue.get_one_queue_details import get_one_queue_details
from app.models.queues.random import RandomQueue
from app.models.responses.queue_details import QueueDetailResponse


class TestGetOneQueueDetails:
    @pytest.fixture(autouse=True)
    def setup(self) -> None:
        self.mock_uuid = uuid4()

        self.mock_queue = RandomQueue(
            name="mock", description="mock", expiry_date=datetime.now()
        )

        self.mock_get_one = Mock(return_value=self.mock_queue)

        self.mock_queue_manager = Mock()
        self.mock_queue_manager.get_one = self.mock_get_one

    def test_get_queue_is_called_with_the_expected_value(self) -> None:
        get_one_queue_details(self.mock_uuid, self.mock_queue_manager)

        self.mock_get_one.assert_called_once_with(self.mock_uuid)

    def test_queue_response(self) -> None:
        result = get_one_queue_details(self.mock_uuid, self.mock_queue_manager)

        assert result == QueueDetailResponse(
            queue_id=self.mock_queue.queue_id,
            name=self.mock_queue.name,
            description=self.mock_queue.description,
            expiry_date=self.mock_queue.expiry_date,
            queue_type=self.mock_queue.queue_type,
            number_of_items=len(self.mock_queue.items),
        )

    def test_handles_no_queue(self) -> None:
        self.mock_get_one.configure_mock(return_value=None)

        with pytest.raises(HTTPException) as exception:
            get_one_queue_details(self.mock_uuid, self.mock_queue_manager)

            assert exception.value.status_code == HTTPStatus.NOT_FOUND
            assert (
                exception.value.detail
                == f"queue with id {self.mock_uuid} was not found"
            )
