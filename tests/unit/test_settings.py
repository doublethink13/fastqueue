from app.settings import Settings


class TestSettings:
    def test_singleton(self) -> None:
        settings1 = Settings.singleton()
        settings2 = Settings.singleton()

        assert id(settings1) == id(settings2)
