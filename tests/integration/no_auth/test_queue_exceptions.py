import json
from uuid import uuid4

from fastapi.testclient import TestClient

from app.models.payloads.create_item import CreateItemPayload


class TestQueueExceptions:
    def test_add_item_to_non_existent_queue(
        self, test_client_no_auth: TestClient
    ) -> None:
        create_item_payload = json.dumps(
            [
                CreateItemPayload(
                    name="mock", description="mock", expiry_date=None, data="mock"
                ).dict()
            ]
        )

        queue_id = uuid4()

        response = test_client_no_auth.post(f"/queue/{queue_id}", create_item_payload)

        assert response.status_code == 404
        assert response.json() == {"detail": f"queue {queue_id} not found"}

    def test_get_non_existent_queue_details(
        self, test_client_no_auth: TestClient
    ) -> None:
        queue_id = uuid4()

        response = test_client_no_auth.get(f"/queue/details/{queue_id}")

        assert response.status_code == 404
        assert response.json() == {"detail": f"queue {queue_id} not found"}

    def test_delete_non_existent_queue(self, test_client_no_auth: TestClient) -> None:
        queue_id = uuid4()

        response = test_client_no_auth.delete(f"/queue/{queue_id}")

        assert response.status_code == 404
        assert response.json() == {"detail": f"queue {queue_id} not found"}
