import json
from typing import Any
from uuid import UUID

from fastapi.testclient import TestClient
from requests import Response

from app.models.enums.queue_type import QueueType
from app.models.payloads.create_item import CreateItemPayload
from app.models.payloads.create_queue import CreateQueuePayload


class TestExpectedUserFlow:
    queue_types = [QueueType.FIFO, QueueType.LIFO, QueueType.Random]
    created_queues_ids: list[str] = []

    def test_create_queues(self, test_client_no_auth: TestClient) -> None:
        responses: list[Response] = [
            test_client_no_auth.post(
                "/queue", self._generate_payload(queue_type).json()
            )
            for queue_type in self.queue_types
        ]

        for index, response in enumerate(responses):
            response_json = response.json()

            self.created_queues_ids.append(str(response_json["id"]))

            self._assert_queue_detail_response(response_json, index)

    def test_get_all_queues_details(self, test_client_no_auth: TestClient) -> None:
        response = test_client_no_auth.get("/queue/details")
        responses: list[Any] = response.json()

        for index, response in enumerate(responses):
            self._assert_queue_detail_response(response, index)

    def test_get_one_queue_details(self, test_client_no_auth: TestClient) -> None:
        created_queue_index = 1

        response = test_client_no_auth.get(
            f"/queue/details/{self.created_queues_ids[created_queue_index]}"
        )

        self._assert_queue_detail_response(response.json(), created_queue_index)

    def test_add_items_to_all_queues(self, test_client_no_auth: TestClient) -> None:
        payloads_as_str = self._create_items_payload()

        responses = self._add_items_to_queues(payloads_as_str, test_client_no_auth)

        for response in responses:
            self._assert_created_items_response(response)

    def test_get_items_from_queues(self, test_client_no_auth: TestClient) -> None:
        self._test_get_one_item_from_all_queues(test_client_no_auth)
        self._test_get_two_items_from_all_queues(test_client_no_auth)

    def test_delete_queues(self, test_client_no_auth: TestClient) -> None:
        for index, queue_id in enumerate(self.created_queues_ids):
            response = test_client_no_auth.delete(f"/queue/{queue_id}")

            self._assert_queue_detail_response(response.json(), index)

    def _generate_payload(self, queue_type: QueueType) -> CreateQueuePayload:
        return CreateQueuePayload(
            name=queue_type.value,
            description=queue_type.value,
            expiry_date=None,
            queue_type=queue_type.value,
        )

    def _create_items_payload(self) -> list[Any]:
        return [
            CreateItemPayload(
                name=str(i), description=str(i), expiry_date=None, data=str(i)
            ).dict()
            for i in range(3)
        ]

    def _assert_queue_detail_response(self, response: Any, index: int) -> None:
        assert response == {
            "id": str(UUID(self.created_queues_ids[index])),
            "name": str(self.queue_types[index].value),
            "description": str(self.queue_types[index].value),
            "expiry_date": None,
            "type": str(self.queue_types[index].value),
            "number_of_items": 0,
        }

    def _add_items_to_queues(
        self, create_items_payload: list[Any], test_client_no_auth: TestClient
    ) -> list[Response]:
        return [
            test_client_no_auth.post(
                f"/queue/{queue_id}", json.dumps(create_items_payload)
            )
            for queue_id in self.created_queues_ids
        ]

    def _assert_created_items_response(self, response: Response) -> None:
        item_ids = [str(UUID(item["id"])) for item in response.json()]

        assert response.json() == [
            {
                "id": item_id,
                "name": str(index),
                "description": str(index),
                "expiry_date": None,
                "data": index,
            }
            for index, item_id in enumerate(item_ids)
        ]

    def _assert_item_response(self, response_json: Any, index: int) -> None:
        assert response_json == {
            "id": str(UUID(response_json["id"])),
            "name": str(index),
            "description": str(index),
            "expiry_date": None,
            "data": index,
        }

    def _test_get_one_item_from_all_queues(
        self, test_client_no_auth: TestClient
    ) -> None:
        for index, queue_type in enumerate(self.queue_types):
            response = test_client_no_auth.get(
                f"/queue/{self.created_queues_ids[index]}"
            )

            response_json: list[Any] = response.json()

            assert len(response_json) == 1

            if queue_type == QueueType.FIFO:
                self._assert_item_response(response_json[0], 0)
            elif queue_type == QueueType.LIFO:
                self._assert_item_response(response_json[0], 2)
            elif queue_type == QueueType.Random:
                self._assert_item_response(response_json[0], response_json[0]["data"])

    def _test_get_two_items_from_all_queues(
        self, test_client_no_auth: TestClient
    ) -> None:
        for index, queue_type in enumerate(self.queue_types):
            response = test_client_no_auth.get(
                f"/queue/{self.created_queues_ids[index]}?number_of_items=2"
            )

            response_json: list[Any] = response.json()

            assert len(response_json) == 2

            if queue_type == QueueType.FIFO:
                self._assert_item_response(response_json[0], 1)
                self._assert_item_response(response_json[1], 2)
            elif queue_type == QueueType.LIFO:
                self._assert_item_response(response_json[0], 1)
                self._assert_item_response(response_json[1], 0)
            elif queue_type == QueueType.Random:
                self._assert_item_response(response_json[0], response_json[0]["data"])
                self._assert_item_response(response_json[1], response_json[1]["data"])
