from fastapi.testclient import TestClient


class TestRootNoAuth:
    def test_root_no_auth(self, test_client_no_auth: TestClient) -> None:
        response = test_client_no_auth.get("")

        assert response.json() == {"message": "Hello, world!"}
