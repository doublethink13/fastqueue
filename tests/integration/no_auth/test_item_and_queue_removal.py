import json
import time
from datetime import datetime
from typing import Generator

import pytest
from fastapi.testclient import TestClient

from app.models.enums.queue_type import QueueType
from app.models.payloads.create_item import CreateItemPayload
from app.models.payloads.create_queue import CreateQueuePayload


class TestItemAndQueueRemoval:
    number_of_items = 5

    @pytest.fixture(autouse=True)
    def setup(self, test_client_no_auth: TestClient) -> Generator[None, None, None]:
        queue_response = test_client_no_auth.post(
            "/queue",
            CreateQueuePayload(
                name="mockqueue",
                description="mockqueue",
                expiry_date=datetime.fromtimestamp(1),
                queue_type=QueueType.Random,
            ).json(),
        ).json()

        create_item_payload = CreateItemPayload(
            name="mockitem",
            description="mockitem",
            expiry_date=datetime.fromtimestamp(0),
            data=[],
        ).dict()
        create_item_payload["expiry_date"] = str(create_item_payload["expiry_date"])

        test_client_no_auth.post(
            f"/queue/{queue_response['id']}",
            json.dumps([create_item_payload for _ in range(self.number_of_items)]),
        ).json()

        yield

        test_client_no_auth.delete(f"/queue/{queue_response['id']}")

    def test_items_are_removed(self, test_client_no_auth: TestClient) -> None:
        self._assert_number_of_items_in_queue(test_client_no_auth, self.number_of_items)

        time.sleep(3)

        self._assert_number_of_items_in_queue(test_client_no_auth, 0)

    def test_queues_are_removed(self, test_client_no_auth: TestClient) -> None:
        self._assert_number_of_queues(test_client_no_auth, 1)

        time.sleep(5)

        self._assert_number_of_queues(test_client_no_auth, 0)

    def _assert_number_of_items_in_queue(
        self, test_client_no_auth: TestClient, number_of_items: int
    ) -> None:
        all_queues_details_response = test_client_no_auth.get("/queue/details").json()

        assert len(all_queues_details_response) == 1
        assert all_queues_details_response[0]["number_of_items"] == number_of_items

    def _assert_number_of_queues(
        self, test_client_no_auth: TestClient, number_of_queues: int
    ) -> None:
        all_queues_details_response = test_client_no_auth.get("/queue/details").json()

        assert len(all_queues_details_response) == number_of_queues
