from typing import Generator

import pytest
from fastapi.testclient import TestClient

from app.main import create_application
from app.models.enums.authentication_mode import AuthenticationMode
from app.models.enums.db_type import DbType
from tests.integration.helpers.reset_env_vars_after_testing import (
    reset_env_var_after_testing,
)
from tests.integration.helpers.setup_env_vars_testing import setup_env_vars_for_testing


@pytest.fixture(scope="session")
def test_client_no_auth() -> Generator[TestClient, None, None]:
    env_backup = setup_env_vars_for_testing(
        {
            "FASTQUEUE_DB_TYPE": DbType.Memory.value,
            "FASTQUEUE_AUTHENTICATION_MODE": AuthenticationMode.Off.value,
            "FASTQUEUE_BEARER_TOKEN_SIGNING_KEY": ""
            + "Noor0ptm0J2X2FJaJcQFkfH7JsiZYP2drSjvGSBQM_k=",
            "FASTQUEUE_REMOVE_ITEMS_TASK_SECONDS": "3",
            "FASTQUEUE_REMOVE_QUEUES_TASK_SECONDS": "5",
        }
    )

    with TestClient(create_application()) as client:
        yield client

    reset_env_var_after_testing(env_backup)
