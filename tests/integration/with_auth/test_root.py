from fastapi.testclient import TestClient


class TestRootWithAuth:
    def test_root_with_auth(self, test_client_auth: TestClient) -> None:
        response = test_client_auth.get("")

        assert response.json() == {"message": "Hello, world!"}
