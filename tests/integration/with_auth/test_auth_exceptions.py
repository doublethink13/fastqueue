import time
from http import HTTPStatus

from fastapi.testclient import TestClient

from app.models.enums.authentication_failed import AuthenticationFailure
from app.models.enums.bearer_token_failed_validation import BearerTokenFailedValidation
from tests.integration.with_auth.helpers.validate_and_return_access_token import (
    validate_and_return_access_token,
)


class TestAuthExceptions:
    def test_access_protected_route_no_auth_headers(
        self, test_client_auth: TestClient
    ) -> None:
        response = test_client_auth.get("/queue/details")

        assert response.status_code == HTTPStatus.UNAUTHORIZED
        assert response.json() == {
            "detail": {
                "message": "Bearer token not valid",
                "reason": BearerTokenFailedValidation.InvalidFormat,
            }
        }

    def test_access_protected_route_wrong_auth_headers(
        self, test_client_auth: TestClient
    ) -> None:
        response = test_client_auth.get(
            "/queue/details", headers={"Authorization": "ups"}
        )

        assert response.status_code == HTTPStatus.UNAUTHORIZED
        assert response.json() == {
            "detail": {
                "message": "Bearer token not valid",
                "reason": BearerTokenFailedValidation.InvalidFormat,
            }
        }

    def test_access_protected_route_wrong_username(
        self, test_client_auth: TestClient
    ) -> None:
        response = test_client_auth.get(
            "/queue/details",
            headers={
                "Authorization": "Bearer "
                + "gAAAAABjkhQ9mCdZpMB43Q6BRVbA0"
                + "-wB6xD6byys0GtWIwSdaqcsjwmC"
                + "X2SkbbaBe8rNzv5KQ8C8pXZx9_m"
                + "e-5yYZEGrGfw4J9BWpzphcZWMC4"
                + "w16zY4lC6U0t5kmixyZP2x7wKlFylG"
            },
        )

        assert response.status_code == HTTPStatus.UNAUTHORIZED
        assert response.json() == {
            "detail": {
                "message": "Bearer token not valid",
                "reason": BearerTokenFailedValidation.InvalidUsername,
            }
        }

    def test_invalid_username(self, test_client_auth: TestClient) -> None:
        response = test_client_auth.post(
            "/auth/token",
            {
                "username": "fastqueue1",
                "password": "averysecretpassword",
                "grant_type": "password",
            },
        )

        assert response.status_code == HTTPStatus.UNAUTHORIZED
        assert response.json() == {
            "detail": {
                "message": "form data not valid",
                "reason": AuthenticationFailure.WrongUsername,
            }
        }

    def test_invalid_password(self, test_client_auth: TestClient) -> None:
        response = test_client_auth.post(
            "/auth/token",
            {
                "username": "fastqueue",
                "password": "averysecretpassword1",
                "grant_type": "password",
            },
        )

        assert response.status_code == HTTPStatus.UNAUTHORIZED
        assert response.json() == {
            "detail": {
                "message": "form data not valid",
                "reason": AuthenticationFailure.WrongPassword,
            }
        }

    def test_access_protected_route_expired_auth_headers(
        self, test_client_auth: TestClient
    ) -> None:
        access_token = validate_and_return_access_token(test_client_auth)

        time.sleep(5)

        response = test_client_auth.get(
            "/queue/details", headers={"Authorization": f"Bearer {access_token}"}
        )

        assert response.status_code == HTTPStatus.UNAUTHORIZED
        assert response.json() == {
            "detail": {
                "message": "Bearer token not valid",
                "reason": BearerTokenFailedValidation.Expired,
            }
        }
