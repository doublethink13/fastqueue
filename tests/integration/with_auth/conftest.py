from typing import Generator

import pytest
from fastapi.testclient import TestClient

from app.main import create_application
from app.models.enums.authentication_mode import AuthenticationMode
from app.models.enums.db_type import DbType
from tests.integration.helpers.reset_env_vars_after_testing import (
    reset_env_var_after_testing,
)
from tests.integration.helpers.setup_env_vars_testing import setup_env_vars_for_testing


@pytest.fixture(scope="function")
def test_client_auth() -> Generator[TestClient, None, None]:
    env_backup = setup_env_vars_for_testing(
        {
            "FASTQUEUE_DB_TYPE": DbType.Memory.value,
            "FASTQUEUE_AUTHENTICATION_MODE": AuthenticationMode.Bearer.value,
            "FASTQUEUE_BEARER_TOKEN_VALIDITY_SECONDS": "5",
            "FASTQUEUE_BEARER_TOKEN_SIGNING_KEY": ""
            + "Noor0ptm0J2X2FJaJcQFkfH7JsiZYP2drSjvGSBQM_k=",
            "FASTQUEUE_BEARER_TOKEN_USERNAME": "fastqueue",
            "FASTQUEUE_BEARER_TOKEN_HASHED_PASSWORD": ""
            + "$2b$12$wbOIFJqNK4YY/cfFkAjkCuic.9YkKXrHLDQIwZAT4c/27ZUMWx6l2",
        }
    )

    with TestClient(create_application()) as client:
        yield client

    reset_env_var_after_testing(env_backup)
