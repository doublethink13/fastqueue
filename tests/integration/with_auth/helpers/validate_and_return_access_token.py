from fastapi.testclient import TestClient

from app.models.managers.auth_manager import AuthManager


def validate_and_return_access_token(test_client: TestClient) -> str:
    response = test_client.post(
        "/auth/token",
        {
            "username": "fastqueue",
            "password": "averysecretpassword",
            "grant_type": "password",
        },
    )

    response_json = response.json()

    auth_manager = AuthManager()

    assert response_json["token_type"] == "bearer"
    assert auth_manager.validate_bearer_token(response_json["access_token"]) is None

    return response_json["access_token"]
