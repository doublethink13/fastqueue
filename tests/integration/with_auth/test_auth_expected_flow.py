from fastapi.testclient import TestClient

from tests.integration.with_auth.helpers.validate_and_return_access_token import (
    validate_and_return_access_token,
)


class TestAuthExpectedFlow:
    def test_get_token(self, test_client_auth: TestClient) -> None:
        validate_and_return_access_token(test_client_auth)

    def test_access_protected_route(self, test_client_auth: TestClient) -> None:
        access_token = validate_and_return_access_token(test_client_auth)

        response = test_client_auth.get(
            "/queue/details", headers={"Authorization": f"Bearer {access_token}"}
        )
        response_json = response.json()

        assert response_json == []
