import pytest


@pytest.fixture
def anyio_backend():
    return "asyncio"


def pytest_collection_modifyitems(items: list[pytest.Item]) -> None:
    tests_in_order = {
        "test_root_no_auth": 0,
        "test_create_queues": 1,
        "test_get_all_queues_details": 2,
        "test_get_one_queue_details": 3,
        "test_add_items_to_all_queues": 4,
        "test_get_items_from_queues": 5,
        "test_delete_queues": 6,
        "test_add_item_to_non_existent_queue": 7,
        "test_get_non_existent_queue_details": 8,
        "test_delete_non_existent_queue": 9,
        "test_items_are_removed": 10,
        "test_queues_are_removed": 11,
        "test_root_with_auth": 12,
        "test_get_token": 13,
        "test_access_protected_route_no_auth_headers": 14,
        "test_access_protected_route_wrong_auth_headers": 15,
        "test_access_protected_route_expired_auth_headers": 16,
        "test_access_protected_route_wrong_username": 17,
        "test_invalid_username": 18,
        "test_invalid_password": 19,
        "test_access_protected_route": 20,
    }

    original_items = items[:]

    for item in original_items:
        items[tests_in_order[item.name]] = item
