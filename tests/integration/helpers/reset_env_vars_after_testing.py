import os


def reset_env_var_after_testing(env_backup: dict[str, str]) -> None:
    for key, value in env_backup.items():
        os.environ[key] = value
