import os


def setup_env_vars_for_testing(env_vars: dict[str, str]) -> dict[str, str]:
    env_backup: dict[str, str] = {}

    for key, value in env_vars.items():
        existing_value = os.environ.get(key, "")
        env_backup[key] = existing_value

        os.environ[key] = value

    return env_backup
