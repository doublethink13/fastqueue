# Technical Overview

A FastAPI app.

Routers:

- queue
- auth

Each queue route has an handler. Each handler needs a manager to handle it's type of data.

## QueueManager

Responsible for interacting with the `DbManager`. Handles operations on queues.

## ItemManager

Responsible for interacting with the `QueueManager`. Handles operations on items.

## DbManager

Responsible for interacting with the `DbInterface`. Handles operations on databases.

## DbInterface

Responsible for communicating with a database.

## Authorization

Every route in the queue router is protected with the `authorize` dependency. Uses an `AutManager` to handle everything auth related.

For now, only bearer tokens are used. They are retrieved using a username and a password.

The `Settings` class has the information about what needs to be defined using env vars.
