# End to end testing

## Manual

- GET / -> `{"message": "Hello, world!"}`
- GET /queue/details (fails due to missing token)
- POST /auth/token (form-data, username and password for Bearer) -> response contains valid token
- Make sure all following requests have an `Authorization` header with the value `Bearer <bearer_token>`
- GET /queue/details -> []
- POST /queue -> queue is created, save ID
- GET /queue/details/`<queue_id>` -> gets queue details
- GET /queue/details -> array with one queue detail
- POST /queue/`<queue_id>` -> created items are returned (should create five items)
- GET /queue/`<queue_id>`?number_of_items=2 -> receives at most 2 items, less if queue does not have enough items to return
- GET /queue/details/`<queue_id>` -> gets queue details
- GET /queue/`<queue_id>`?number_of_items=2 -> receives at most 2 items, less if queue does not have enough items to return
- GET /queue/`<queue_id>`?number_of_items=2 -> receives at most 2 items, less if queue does not have enough items to return
- DELETE /queue/`<queue_id>` -> queue is deleted
- GET /queue/details -> []
- POST /queue -> queue is created, save ID
- GET /queue/details/`<queue_id>` -> gets queue details (expiry_date needs to be set)
- POST /queue/`<queue_id>` -> created items are returned (should create five items) (expiry_date needs to be set for some items, but not all)
- Wait 60 seconds, try to get queue -> this fails, because queue was removed by job.
- POST /queue -> queue is created, save ID
- GET /queue/details/`<queue_id>` -> gets queue details (expiry_date can't be set)
- POST /queue/`<queue_id>` -> created items are returned (should create five items) (expiry_date needs to be set for some items, but not all)
- Wait 60 seconds, try to get queue details -> items with expiry_date should have been removed
