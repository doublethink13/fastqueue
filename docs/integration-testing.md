# Integration testing

Using pytest + Starlette TestClient

## Expected user flows

- Create one queue of each type
- Get single queue details
- Get all queues details
- Add single items to each queue
- Retrieve item(s) from each queue
- Delete queue

## Handle exceptions

- Adding items to non-existent queues
- Getting items from empty queues
- Get unexistent queue details
- Deleting unexistent queue

## Add different types of data in item

TODO
